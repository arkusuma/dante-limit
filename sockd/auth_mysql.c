#include <stdio.h>
#include <mysql/mysql.h>
#include "common.h"
#include "sha1.h"

struct user {
   char username[21];
   char password[41];
   unsigned short active;
   unsigned long active_start;
   unsigned long active_last;
   unsigned long client_ip;
   unsigned char locked;
};

static int
select_user(MYSQL *conn, const char *username, struct user *user);

static void
sha1(text, hex)
   const char *text;
   char *hex;
{
   unsigned char digest[SHA1HashSize];
   SHA1Context ctx;
   int i;

   SHA1Reset(&ctx);
   SHA1Input(&ctx, text, strlen(text));
   SHA1Result(&ctx, digest);

   for (i = 0; i < SHA1HashSize; i++)
      hex += sprintf(hex, "%02x", digest[i]);
}

void
mysql_reset_auth()
{
   MYSQL *conn;
   char sql[256];
	 
   snprintf(sql, sizeof(sql) - 1,
       "update %s set active=0, active_start=0, active_last=0",
       sockscf.authtable);
   sql[sizeof(sql) - 1] = 0;

   conn = mysql_init(0);
   if (conn == 0)
      goto error;

   if (mysql_real_connect(conn, sockscf.authserver, sockscf.authusername,
         sockscf.authpassword, sockscf.authdatabase, 0, 0, 0) == 0)
      goto error;

   if (mysql_query(conn, sql) != 0)
      goto error;

   mysql_close(conn);
   return;

error:
   if (conn != 0) {
      slog(LOG_ERR, "%s", mysql_error(conn));
      mysql_close(conn);
   }
}

int
mysql_passwordcheck(username, password, src)
   const char *username;
   const char *password;
   const struct sockaddr *src;
{
   MYSQL *conn;
   struct user user;
   const struct sockaddr_in *addr = TOCIN(src);
   char password_hash[SHA1HashSize * 2 + 1];
   unsigned long client_ip = addr->sin_addr.s_addr;
   int ret = -1;

   conn = mysql_init(0);
   if (conn == 0)
      goto error;

   if (mysql_real_connect(conn, sockscf.authserver, sockscf.authusername,
         sockscf.authpassword, sockscf.authdatabase, 0, 0, 0) == 0)
      goto error;

   if (select_user(conn, username, &user) != 0)
      goto error;

   sha1(password, password_hash);
   if (!user.locked && strcmp(user.password, password_hash) == 0) {
      if (!user.active) {
         if (user.client_ip == client_ip
         ||  user.active_last + sockscf.authdelay <= time(0)) {
            ret = 0;
         }
      } else {
         if (user.client_ip == client_ip) {
            ret = 0;
         }
      }
   }

   mysql_close(conn);
   return ret;

error:
   if (conn != 0)
      mysql_close(conn);
   return ret;
}

static int
select_user(conn, username, user)
   MYSQL *conn;
   const char *username;
   struct user *user;
{
   MYSQL_STMT *stmt;
   MYSQL_BIND param[1], result[7];
   char sql[256];
	
   snprintf(sql, sizeof(sql) - 1, "select * from %s where username=?",
       sockscf.authtable);
   sql[sizeof(sql) - 1] = 0;

   stmt = mysql_stmt_init(conn);
   if (stmt == NULL)
      goto error;

   if (mysql_stmt_prepare(stmt, sql, strlen(sql)) != 0)
      goto error;

   /* Bind parameter */
   memset(param, 0, sizeof(param));
   param[0].buffer_type = MYSQL_TYPE_STRING;
   param[0].buffer = (void *) username;
   param[0].buffer_length = strlen(username);

   if (mysql_stmt_bind_param(stmt, param) != 0)
      goto error;

   /* Bind result columns */
   memset(user, 0, sizeof(*user));
   memset(result, 0, sizeof(result));

   result[0].buffer_type = MYSQL_TYPE_STRING;
   result[0].buffer = (void *) user->username;
   result[0].buffer_length = sizeof(user->username) - 1;

   result[1].buffer_type = MYSQL_TYPE_STRING;
   result[1].buffer = (void *) user->password;
   result[1].buffer_length = sizeof(user->password) - 1;

   result[2].buffer_type = MYSQL_TYPE_SHORT;
   result[2].buffer = (void *) &user->active;
   result[2].is_unsigned = 1;

   result[3].buffer_type = MYSQL_TYPE_LONG;
   result[3].buffer = (void *) &user->active_start;
   result[3].is_unsigned = 1;

   result[4].buffer_type = MYSQL_TYPE_LONG;
   result[4].buffer = (void *) &user->active_last;
   result[4].is_unsigned = 1;

   result[5].buffer_type = MYSQL_TYPE_LONG;
   result[5].buffer = (void *) &user->client_ip;
   result[5].is_unsigned = 1;

   result[6].buffer_type = MYSQL_TYPE_TINY;
   result[6].buffer = (void *) &user->locked;
   result[6].is_unsigned = 1;

   if (mysql_stmt_bind_result(stmt, result) != 0)
      goto error;

   if (mysql_stmt_execute(stmt) != 0)
      goto error;

   if (mysql_stmt_fetch(stmt) != 0)
      goto error;

   mysql_stmt_close(stmt);
   return 0;

error:
   slog(LOG_ERR, "%s", mysql_error(conn));
   if (stmt != 0)
      mysql_stmt_close(stmt);
   return -1;
}

static int
new_active(conn, username, client_ip)
   MYSQL *conn;
   const char *username;
   unsigned int client_ip;
{
   int ret = 0;
   MYSQL_STMT *stmt;
   MYSQL_BIND param[4];
   unsigned long active_start = (unsigned long) time(0);
   unsigned long active_last = active_start;
   char sql[256];

   snprintf(sql, sizeof(sql) - 1,
       "update %s set active=1, active_start=?, active_last=?, client_ip=? where username=?",
       sockscf.authtable);
   sql[sizeof(sql) - 1] = 0;

   stmt = mysql_stmt_init(conn);
   if (stmt == NULL)
      goto error;

   if (mysql_stmt_prepare(stmt, sql, strlen(sql)) != 0)
      goto error;

   /* Bind parameter */
   memset(param, 0, sizeof(param));

   param[0].buffer_type = MYSQL_TYPE_LONG;
   param[0].buffer = (void *) &active_start;
   param[0].is_unsigned = 1;

   param[1].buffer_type = MYSQL_TYPE_LONG;
   param[1].buffer = (void *) &active_last;
   param[1].is_unsigned = 1;

   param[2].buffer_type = MYSQL_TYPE_LONG;
   param[2].buffer = (void *) &client_ip;
   param[2].is_unsigned = 1;

   param[3].buffer_type = MYSQL_TYPE_STRING;
   param[3].buffer = (void *) username;
   param[3].buffer_length = strlen(username);

   if (mysql_stmt_bind_param(stmt, param) != 0)
      goto error;

   if (mysql_stmt_execute(stmt) != 0)
      goto error;

   if (mysql_stmt_affected_rows(stmt) != 1)
      ret = -1;

   mysql_stmt_close(stmt);
   return ret;

error:
   slog(LOG_ERR, "%s", mysql_error(conn));
   if (stmt != 0)
      mysql_stmt_close(stmt);
   return -1;
}

static int
set_active(conn, username, active)
   MYSQL *conn;
   const char *username;
   int active;
{
   int ret = 0;
   MYSQL_STMT *stmt;
   MYSQL_BIND param[3];
   unsigned long active_last = (unsigned long) time(0);
   char sql[256];

   snprintf(sql, sizeof(sql) - 1,
       "update %s set active=?, active_last=? where username=?",
       sockscf.authtable);
   sql[sizeof(sql) - 1] = 0;

   stmt = mysql_stmt_init(conn);
   if (stmt == NULL)
      goto error;

   if (mysql_stmt_prepare(stmt, sql, strlen(sql)) != 0)
      goto error;

   /* Bind parameter */
   memset(param, 0, sizeof(param));

   param[0].buffer_type = MYSQL_TYPE_LONG;
   param[0].buffer = (void *) &active;

   param[1].buffer_type = MYSQL_TYPE_LONG;
   param[1].buffer = (void *) &active_last;
   param[1].is_unsigned = 1;

   param[2].buffer_type = MYSQL_TYPE_STRING;
   param[2].buffer = (void *) username;
   param[2].buffer_length = strlen(username);

   if (mysql_stmt_bind_param(stmt, param) != 0)
      goto error;

   if (mysql_stmt_execute(stmt) != 0)
      goto error;

   if (mysql_stmt_affected_rows(stmt) != 1)
      ret = -1;

   mysql_stmt_close(stmt);
   return ret;

error:
   slog(LOG_ERR, "%s", mysql_error(conn));
   if (stmt != 0)
      mysql_stmt_close(stmt);
   return -1;
}

int
mark_user_active(username, src)
   const char *username;
   const struct sockaddr *src;
{
   MYSQL *conn;
   struct user user;
   const struct sockaddr_in *addr = TOCIN(src);
   unsigned long client_ip = addr->sin_addr.s_addr;
   int ret = -1;

   conn = mysql_init(0);
   if (conn == 0)
      goto error;

   if (mysql_real_connect(conn, sockscf.authserver, sockscf.authusername,
         sockscf.authpassword, sockscf.authdatabase, 0, 0, 0) == 0)
      goto error;

   if (mysql_query(conn, "begin") != 0)
      goto error;

   if (select_user(conn, username, &user) != 0)
      goto error;

   if (!user.active) {
      if (user.client_ip == client_ip
      ||  user.active_last + sockscf.authdelay <= time(0)) {
         ret = new_active(conn, username, client_ip);
      }
   } else {
      if (user.client_ip == client_ip) {
         ret = set_active(conn, username, user.active + 1);
      }
   }

   if (mysql_commit(conn) != 0)
      goto error;

   mysql_close(conn);
   return ret;

error:
   if (conn != 0)
      mysql_close(conn);
   return ret;
}

int
mark_user_inactive(username, src)
   const char *username;
   const struct sockaddr *src;
{
   MYSQL *conn;
   struct user user;
   int ret = 0;
   const struct sockaddr_in *addr = TOCIN(src);
   unsigned long client_ip = addr->sin_addr.s_addr;

   conn = mysql_init(0);
   if (conn == 0)
      goto error;

   if (mysql_real_connect(conn, sockscf.authserver, sockscf.authusername,
         sockscf.authpassword, sockscf.authdatabase, 0, 0, 0) == 0)
      goto error;

   if (mysql_query(conn, "begin") != 0)
      goto error;

   if (select_user(conn, username, &user) != 0)
      goto error;

	 if (user.active > 0)
      set_active(conn, username, user.active - 1);

   if (mysql_commit(conn) != 0)
      goto error;

   mysql_close(conn);
   return ret;

error:
   if (conn != 0)
      mysql_close(conn);
   return -1;
}
