--
-- Database: `dante`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
	`username` varchar(20) NOT NULL,
	`password` varchar(40) NOT NULL COMMENT 'Password in SHA1',
	`active` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT 'User currently active?',
	`active_start` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Starting time of user become active',
	`active_last` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'The last time user active',
	`client_ip` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Client IP address',
	`locked` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Locked user cannot use proxy',
	PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users` (`username`, `password`, `active`, `active_start`, `active_last`, `client_ip`, `locked`) VALUES
('test', '601f1889667efaebb33b8c12572835da3f027f78', 0, 0, 0, 0, 0);
