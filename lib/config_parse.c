#include "common.h"
/* A Bison parser, made by GNU Bison 2.5.  */

/* Bison implementation for Yacc-like parsers in C
   
      Copyright (C) 1984, 1989-1990, 2000-2011 Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.5"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Using locations.  */
#define YYLSP_NEEDED 0

/* Substitute the variable and function names.  */
#define yyparse         socks_yyparse
#define yylex           socks_yylex
#define yyerror         socks_yyerror
#define yylval          socks_yylval
#define yychar          socks_yychar
#define yydebug         socks_yydebug
#define yynerrs         socks_yynerrs


/* Copy the first part of user declarations.  */

/* Line 268 of yacc.c  */
#line 45 "config_parse.y"


#if 0 /* XXX automatically added at head of generated .c file */
#include "common.h"
#endif
#include "ifaddrs_compat.h"
#include "yacconfig.h"

static const char rcsid[] =
"$Id: config_parse.y,v 1.401 2011/08/01 15:23:27 michaels Exp $";

#if HAVE_LIBWRAP && (!SOCKS_CLIENT)
   extern jmp_buf tcpd_buf;
#endif /* HAVE_LIBWRAP && (!SOCKS_CLIENT) */

#define CHECKNUMBER(number, op, checkagainst)                                  \
do {                                                                           \
   if (!(atol((number)) op (checkagainst)))                                    \
      yyerror("number must be " #op " " #checkagainst ".  It can not be %ld",  \
              atol((number)));                                                 \
} while (0)

#define CHECKPORTNUMBER(portnumber)                                            \
do {                                                                           \
      CHECKNUMBER(portnumber, >=, 0);                                          \
      CHECKNUMBER(portnumber, <=, IP_MAXPORT);                                 \
} while (0)

static void
addrinit(struct ruleaddr_t *addr, const int netmask_required);

static void
gwaddrinit(gwaddr_t *addr);

#if SOCKS_CLIENT
static void parseclientenv(int *haveproxyserver);
/*
 * parses client environment, if any.  
 * If a proxyserver is configured in enviroment, "haveproxyserver" is set
 * to true upon return.  If not, it is set to false.
 */

static void
addproxyserver(const char *proxyserver, 
               const struct proxyprotocol_t *proxyprotocol);
/*
 * Adds a route for a proxyserver with address "proxyserver" to our
 * routes.
 * "proxyprotocol" is the proxyprotocols supported by the proxyserver.
 */
#else /* !SOCKS_CLIENT */

/*
 * Reset pointers to point away from rule-specific memory to global
 * memory.  Should be called after adding a rule.
 */
static void rulereset(void);

/*
 * Prepare pointers to point to the correct memory for adding a new rule.
 */
static void ruleinit(struct rule_t *rule);

#endif /* !SOCKS_CLIENT */

extern int yylineno;
extern char *yytext;

static int parsingconfig;

static unsigned char          add_to_errorlog; /* adding logfile to errorlog? */
static struct timeout_t       *timeout = &sockscf.timeout;

#if !SOCKS_CLIENT
static struct rule_t          rule;          /* new rule.                     */
static struct protocol_t      protocolmem;   /* new protocolmem.              */
#if !HAVE_PRIVILEGES
static struct userid_t        olduserid;
#endif /* !HAVE_PRIVILEGES */
#endif /* !SOCKS_CLIENT */

static struct serverstate_t   state;
static struct route_t         route;         /* new route.                    */
static gwaddr_t               gw;            /* new gateway.                  */

static struct ruleaddr_t      src;            /* new src.                     */
static struct ruleaddr_t      dst;            /* new dst.                     */
static struct ruleaddr_t      rdr_from;
static struct ruleaddr_t      rdr_to;

#if BAREFOOTD
static struct ruleaddr_t      bounce_to;
#endif /* BAREFOOTD */

static struct ruleaddr_t      *ruleaddr;      /* current ruleaddr             */
static struct extension_t     *extension;     /* new extensions               */
static struct proxyprotocol_t *proxyprotocol; /* proxy protocol.              */

static atype_t                *atype;         /* atype of new address.        */
static struct in_addr         *ipaddr;        /* new ip address               */
static struct in_addr         *netmask;       /* new netmask                  */
static int                    netmask_required;/*
                                                * netmask required for this
                                                * address?
                                                */
static char                   *domain;        /* new domain.                  */
static char                   *ifname;        /* new ifname.                  */
static char                   *url;           /* new url.                     */

static in_port_t              *port_tcp;      /* new TCP port number.         */
static in_port_t              *port_udp;      /* new UDP port number.         */
static int                    *methodv;       /* new authmethods.             */
static size_t                 *methodc;       /* number of them.              */
static struct protocol_t      *protocol;      /* new protocol.                */
static struct command_t       *command;       /* new command.                 */
static enum operator_t        *operator;      /* new operator.                */

#if HAVE_GSSAPI
static char                  *gssapiservicename; /* new gssapiservice.        */
static char                  *gssapikeytab;      /* new gssapikeytab.         */
static struct gssapi_enc_t   *gssapiencryption;  /* new encryption status.    */
#endif /* HAVE_GSSAPI */

#if HAVE_LDAP
static struct ldap_t         *ldap;        /* new ldap server details.        */
#endif

#if DEBUG
#define YYDEBUG 1
#endif /* DEBUG */

#define ADDMETHOD(method)                                                      \
do {                                                                           \
   if (methodisset(method, methodv, *methodc))                                 \
      yywarn("duplicate method: %s", method2string(method));                   \
   else {                                                                      \
      if (*methodc >= MAXMETHOD)                                               \
         yyerror("internal error, too many authmethods (%ld >= %ld)",          \
         (long)*methodc, (long)MAXMETHOD);                                     \
      methodv[(*methodc)++] = method;                                          \
   }                                                                           \
} while (0)



/* Line 268 of yacc.c  */
#line 225 "config_parse.c"

/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     SERVERCONFIG = 258,
     CLIENTCONFIG = 259,
     DEPRECATED = 260,
     CLIENTRULE = 261,
     INTERNAL = 262,
     EXTERNAL = 263,
     REALM = 264,
     REALNAME = 265,
     EXTERNAL_ROTATION = 266,
     SAMESAME = 267,
     DEBUGGING = 268,
     RESOLVEPROTOCOL = 269,
     SOCKET = 270,
     CLIENTSIDE_SOCKET = 271,
     SNDBUF = 272,
     RCVBUF = 273,
     SRCHOST = 274,
     NODNSMISMATCH = 275,
     NODNSUNKNOWN = 276,
     CHECKREPLYAUTH = 277,
     EXTENSION = 278,
     BIND = 279,
     PRIVILEGED = 280,
     IOTIMEOUT = 281,
     IOTIMEOUT_TCP = 282,
     IOTIMEOUT_UDP = 283,
     NEGOTIATETIMEOUT = 284,
     CONNECTTIMEOUT = 285,
     TCP_FIN_WAIT = 286,
     METHOD = 287,
     CLIENTMETHOD = 288,
     NONE = 289,
     GSSAPI = 290,
     UNAME = 291,
     RFC931 = 292,
     PAM = 293,
     BSDAUTH = 294,
     COMPATIBILITY = 295,
     SAMEPORT = 296,
     DRAFT_5_05 = 297,
     CLIENTCOMPATIBILITY = 298,
     NECGSSAPI = 299,
     USERNAME = 300,
     GROUPNAME = 301,
     USER_PRIVILEGED = 302,
     USER_UNPRIVILEGED = 303,
     USER_LIBWRAP = 304,
     LIBWRAP_FILE = 305,
     ERRORLOG = 306,
     LOGOUTPUT = 307,
     LOGFILE = 308,
     CHILD_MAXIDLE = 309,
     CHILD_MAXREQUESTS = 310,
     ROUTE = 311,
     VIA = 312,
     BADROUTE_EXPIRE = 313,
     MAXFAIL = 314,
     VERDICT_BLOCK = 315,
     VERDICT_PASS = 316,
     PAMSERVICENAME = 317,
     BSDAUTHSTYLENAME = 318,
     BSDAUTHSTYLE = 319,
     GSSAPISERVICE = 320,
     GSSAPIKEYTAB = 321,
     GSSAPIENCTYPE = 322,
     GSSAPIENC_ANY = 323,
     GSSAPIENC_CLEAR = 324,
     GSSAPIENC_INTEGRITY = 325,
     GSSAPIENC_CONFIDENTIALITY = 326,
     GSSAPIENC_PERMESSAGE = 327,
     GSSAPISERVICENAME = 328,
     GSSAPIKEYTABNAME = 329,
     PROTOCOL = 330,
     PROTOCOL_TCP = 331,
     PROTOCOL_UDP = 332,
     PROTOCOL_FAKE = 333,
     PROXYPROTOCOL = 334,
     PROXYPROTOCOL_SOCKS_V4 = 335,
     PROXYPROTOCOL_SOCKS_V5 = 336,
     PROXYPROTOCOL_HTTP = 337,
     PROXYPROTOCOL_UPNP = 338,
     USER = 339,
     GROUP = 340,
     COMMAND = 341,
     COMMAND_BIND = 342,
     COMMAND_CONNECT = 343,
     COMMAND_UDPASSOCIATE = 344,
     COMMAND_BINDREPLY = 345,
     COMMAND_UDPREPLY = 346,
     ACTION = 347,
     LINE = 348,
     LIBWRAPSTART = 349,
     LIBWRAP_ALLOW = 350,
     LIBWRAP_DENY = 351,
     LIBWRAP_HOSTS_ACCESS = 352,
     OPERATOR = 353,
     SOCKS_LOG = 354,
     SOCKS_LOG_CONNECT = 355,
     SOCKS_LOG_DATA = 356,
     SOCKS_LOG_DISCONNECT = 357,
     SOCKS_LOG_ERROR = 358,
     SOCKS_LOG_IOOPERATION = 359,
     IPADDRESS = 360,
     DOMAINNAME = 361,
     DIRECT = 362,
     IFNAME = 363,
     URL = 364,
     PORT = 365,
     SERVICENAME = 366,
     NUMBER = 367,
     FROM = 368,
     TO = 369,
     REDIRECT = 370,
     BANDWIDTH = 371,
     MAXSESSIONS = 372,
     UDPPORTRANGE = 373,
     UDPCONNECTDST = 374,
     YES = 375,
     NO = 376,
     BOUNCE = 377,
     LDAPURL = 378,
     LDAP_URL = 379,
     LDAPSSL = 380,
     LDAPCERTCHECK = 381,
     LDAPKEEPREALM = 382,
     LDAPBASEDN = 383,
     LDAP_BASEDN = 384,
     LDAPBASEDN_HEX = 385,
     LDAPBASEDN_HEX_ALL = 386,
     LDAPSERVER = 387,
     LDAPSERVER_NAME = 388,
     LDAPGROUP = 389,
     LDAPGROUP_NAME = 390,
     LDAPGROUP_HEX = 391,
     LDAPGROUP_HEX_ALL = 392,
     LDAPFILTER = 393,
     LDAPFILTER_AD = 394,
     LDAPFILTER_HEX = 395,
     LDAPFILTER_AD_HEX = 396,
     LDAPATTRIBUTE = 397,
     LDAPATTRIBUTE_AD = 398,
     LDAPATTRIBUTE_HEX = 399,
     LDAPATTRIBUTE_AD_HEX = 400,
     LDAPCERTFILE = 401,
     LDAPCERTPATH = 402,
     LDAPPORT = 403,
     LDAPPORTSSL = 404,
     LDAP_FILTER = 405,
     LDAP_ATTRIBUTE = 406,
     LDAP_CERTFILE = 407,
     LDAP_CERTPATH = 408,
     LDAPDOMAIN = 409,
     LDAP_DOMAIN = 410,
     LDAPTIMEOUT = 411,
     LDAPCACHE = 412,
     LDAPCACHEPOS = 413,
     LDAPCACHENEG = 414,
     LDAPKEYTAB = 415,
     LDAPKEYTABNAME = 416,
     LDAPDEADTIME = 417,
     LDAPDEBUG = 418,
     LDAPDEPTH = 419,
     LDAPAUTO = 420,
     LDAPSEARCHTIME = 421,
     AUTHSERVER = 422,
     AUTHUSERNAME = 423,
     AUTHPASSWORD = 424,
     AUTHDATABASE = 425,
     AUTHTABLE = 426,
     AUTHDELAY = 427,
     AUTH_STRING = 428
   };
#endif
/* Tokens.  */
#define SERVERCONFIG 258
#define CLIENTCONFIG 259
#define DEPRECATED 260
#define CLIENTRULE 261
#define INTERNAL 262
#define EXTERNAL 263
#define REALM 264
#define REALNAME 265
#define EXTERNAL_ROTATION 266
#define SAMESAME 267
#define DEBUGGING 268
#define RESOLVEPROTOCOL 269
#define SOCKET 270
#define CLIENTSIDE_SOCKET 271
#define SNDBUF 272
#define RCVBUF 273
#define SRCHOST 274
#define NODNSMISMATCH 275
#define NODNSUNKNOWN 276
#define CHECKREPLYAUTH 277
#define EXTENSION 278
#define BIND 279
#define PRIVILEGED 280
#define IOTIMEOUT 281
#define IOTIMEOUT_TCP 282
#define IOTIMEOUT_UDP 283
#define NEGOTIATETIMEOUT 284
#define CONNECTTIMEOUT 285
#define TCP_FIN_WAIT 286
#define METHOD 287
#define CLIENTMETHOD 288
#define NONE 289
#define GSSAPI 290
#define UNAME 291
#define RFC931 292
#define PAM 293
#define BSDAUTH 294
#define COMPATIBILITY 295
#define SAMEPORT 296
#define DRAFT_5_05 297
#define CLIENTCOMPATIBILITY 298
#define NECGSSAPI 299
#define USERNAME 300
#define GROUPNAME 301
#define USER_PRIVILEGED 302
#define USER_UNPRIVILEGED 303
#define USER_LIBWRAP 304
#define LIBWRAP_FILE 305
#define ERRORLOG 306
#define LOGOUTPUT 307
#define LOGFILE 308
#define CHILD_MAXIDLE 309
#define CHILD_MAXREQUESTS 310
#define ROUTE 311
#define VIA 312
#define BADROUTE_EXPIRE 313
#define MAXFAIL 314
#define VERDICT_BLOCK 315
#define VERDICT_PASS 316
#define PAMSERVICENAME 317
#define BSDAUTHSTYLENAME 318
#define BSDAUTHSTYLE 319
#define GSSAPISERVICE 320
#define GSSAPIKEYTAB 321
#define GSSAPIENCTYPE 322
#define GSSAPIENC_ANY 323
#define GSSAPIENC_CLEAR 324
#define GSSAPIENC_INTEGRITY 325
#define GSSAPIENC_CONFIDENTIALITY 326
#define GSSAPIENC_PERMESSAGE 327
#define GSSAPISERVICENAME 328
#define GSSAPIKEYTABNAME 329
#define PROTOCOL 330
#define PROTOCOL_TCP 331
#define PROTOCOL_UDP 332
#define PROTOCOL_FAKE 333
#define PROXYPROTOCOL 334
#define PROXYPROTOCOL_SOCKS_V4 335
#define PROXYPROTOCOL_SOCKS_V5 336
#define PROXYPROTOCOL_HTTP 337
#define PROXYPROTOCOL_UPNP 338
#define USER 339
#define GROUP 340
#define COMMAND 341
#define COMMAND_BIND 342
#define COMMAND_CONNECT 343
#define COMMAND_UDPASSOCIATE 344
#define COMMAND_BINDREPLY 345
#define COMMAND_UDPREPLY 346
#define ACTION 347
#define LINE 348
#define LIBWRAPSTART 349
#define LIBWRAP_ALLOW 350
#define LIBWRAP_DENY 351
#define LIBWRAP_HOSTS_ACCESS 352
#define OPERATOR 353
#define SOCKS_LOG 354
#define SOCKS_LOG_CONNECT 355
#define SOCKS_LOG_DATA 356
#define SOCKS_LOG_DISCONNECT 357
#define SOCKS_LOG_ERROR 358
#define SOCKS_LOG_IOOPERATION 359
#define IPADDRESS 360
#define DOMAINNAME 361
#define DIRECT 362
#define IFNAME 363
#define URL 364
#define PORT 365
#define SERVICENAME 366
#define NUMBER 367
#define FROM 368
#define TO 369
#define REDIRECT 370
#define BANDWIDTH 371
#define MAXSESSIONS 372
#define UDPPORTRANGE 373
#define UDPCONNECTDST 374
#define YES 375
#define NO 376
#define BOUNCE 377
#define LDAPURL 378
#define LDAP_URL 379
#define LDAPSSL 380
#define LDAPCERTCHECK 381
#define LDAPKEEPREALM 382
#define LDAPBASEDN 383
#define LDAP_BASEDN 384
#define LDAPBASEDN_HEX 385
#define LDAPBASEDN_HEX_ALL 386
#define LDAPSERVER 387
#define LDAPSERVER_NAME 388
#define LDAPGROUP 389
#define LDAPGROUP_NAME 390
#define LDAPGROUP_HEX 391
#define LDAPGROUP_HEX_ALL 392
#define LDAPFILTER 393
#define LDAPFILTER_AD 394
#define LDAPFILTER_HEX 395
#define LDAPFILTER_AD_HEX 396
#define LDAPATTRIBUTE 397
#define LDAPATTRIBUTE_AD 398
#define LDAPATTRIBUTE_HEX 399
#define LDAPATTRIBUTE_AD_HEX 400
#define LDAPCERTFILE 401
#define LDAPCERTPATH 402
#define LDAPPORT 403
#define LDAPPORTSSL 404
#define LDAP_FILTER 405
#define LDAP_ATTRIBUTE 406
#define LDAP_CERTFILE 407
#define LDAP_CERTPATH 408
#define LDAPDOMAIN 409
#define LDAP_DOMAIN 410
#define LDAPTIMEOUT 411
#define LDAPCACHE 412
#define LDAPCACHEPOS 413
#define LDAPCACHENEG 414
#define LDAPKEYTAB 415
#define LDAPKEYTABNAME 416
#define LDAPDEADTIME 417
#define LDAPDEBUG 418
#define LDAPDEPTH 419
#define LDAPAUTO 420
#define LDAPSEARCHTIME 421
#define AUTHSERVER 422
#define AUTHUSERNAME 423
#define AUTHPASSWORD 424
#define AUTHDATABASE 425
#define AUTHTABLE 426
#define AUTHDELAY 427
#define AUTH_STRING 428




#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{

/* Line 293 of yacc.c  */
#line 190 "config_parse.y"

   char   *string;
   uid_t   uid;



/* Line 293 of yacc.c  */
#line 614 "config_parse.c"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif


/* Copy the second part of user declarations.  */


/* Line 343 of yacc.c  */
#line 626 "config_parse.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int yyi)
#else
static int
YYID (yyi)
    int yyi;
#endif
{
  return yyi;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)				\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack_alloc, Stack, yysize);			\
	Stack = &yyptr->Stack_alloc;					\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  6
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   639

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  181
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  176
/* YYNRULES -- Number of rules.  */
#define YYNRULES  361
/* YYNRULES -- Number of states.  */
#define YYNSTATES  601

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   428

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     174,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,   179,   178,   180,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,   177,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,   175,     2,   176,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,   146,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,   162,   163,   164,
     165,   166,   167,   168,   169,   170,   171,   172,   173
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     6,     9,    11,    14,    15,    18,    20,
      22,    24,    25,    28,    31,    34,    36,    38,    40,    42,
      45,    47,    49,    51,    53,    55,    57,    59,    61,    63,
      65,    67,    69,    71,    73,    75,    77,    79,    81,    83,
      85,    87,    89,    91,    93,    95,    97,    99,   101,   103,
     105,   107,   109,   111,   113,   122,   123,   127,   129,   131,
     133,   135,   137,   139,   142,   146,   148,   150,   153,   157,
     159,   161,   164,   168,   170,   172,   175,   180,   181,   186,
     187,   191,   195,   199,   201,   203,   205,   207,   209,   215,
     221,   222,   227,   228,   233,   235,   237,   240,   244,   248,
     252,   254,   256,   258,   262,   266,   270,   272,   276,   280,
     284,   288,   292,   296,   300,   302,   304,   308,   312,   316,
     320,   324,   328,   332,   334,   336,   338,   341,   345,   347,
     349,   351,   359,   367,   375,   383,   391,   399,   403,   405,
     407,   409,   411,   414,   418,   422,   424,   427,   428,   433,
     434,   439,   441,   443,   445,   447,   449,   451,   459,   461,
     463,   465,   467,   468,   471,   478,   480,   482,   484,   486,
     488,   490,   492,   493,   496,   498,   500,   502,   504,   506,
     508,   510,   512,   514,   516,   518,   520,   522,   524,   526,
     528,   530,   532,   534,   536,   538,   540,   542,   544,   546,
     548,   550,   552,   554,   556,   558,   560,   562,   564,   566,
     568,   570,   572,   574,   576,   578,   580,   584,   589,   593,
     597,   601,   605,   609,   613,   617,   621,   625,   629,   633,
     637,   641,   645,   649,   653,   657,   661,   665,   669,   673,
     677,   681,   685,   689,   693,   697,   701,   705,   709,   713,
     717,   719,   721,   724,   726,   728,   732,   734,   737,   739,
     741,   743,   745,   747,   751,   753,   756,   758,   760,   763,
     767,   770,   773,   775,   779,   783,   787,   789,   791,   793,
     795,   797,   799,   802,   806,   810,   814,   818,   822,   824,
     826,   828,   830,   832,   834,   837,   842,   846,   850,   854,
     858,   862,   866,   868,   870,   872,   874,   876,   878,   880,
     882,   884,   885,   888,   890,   892,   894,   896,   898,   900,
     902,   904,   906,   911,   912,   916,   919,   922,   925,   928,
     931,   933,   935,   937,   939,   941,   943,   945,   947,   949,
     950,   954,   958,   961,   962,   966,   968,   970,   974,   976,
     978,   980,   982,   988,   990,   992,   996,  1000,  1004,  1007,
    1011,  1015
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int16 yyrhs[] =
{
     182,     0,    -1,   183,   184,    -1,   188,   187,    -1,     3,
      -1,   190,   185,    -1,    -1,   186,   185,    -1,   255,    -1,
     258,    -1,   195,    -1,    -1,   187,   174,    -1,   187,   189,
      -1,   187,   195,    -1,     4,    -1,   214,    -1,   194,    -1,
     191,    -1,   190,   191,    -1,   250,    -1,   252,    -1,   194,
      -1,   209,    -1,   211,    -1,   213,    -1,   216,    -1,   218,
      -1,   192,    -1,   223,    -1,   222,    -1,   232,    -1,   233,
      -1,   236,    -1,   237,    -1,   351,    -1,   352,    -1,   353,
      -1,   354,    -1,   355,    -1,   356,    -1,   238,    -1,   206,
      -1,   215,    -1,   241,    -1,   247,    -1,   243,    -1,   244,
      -1,   193,    -1,   230,    -1,   228,    -1,   229,    -1,   231,
      -1,     5,    -1,    56,   196,   175,   323,   300,   321,   323,
     176,    -1,    -1,    79,   177,   199,    -1,    80,    -1,    81,
      -1,    82,    -1,    83,    -1,   194,    -1,   198,    -1,   198,
     199,    -1,    84,   177,   202,    -1,    45,    -1,   201,    -1,
     201,   202,    -1,    85,   177,   205,    -1,    46,    -1,   204,
      -1,   204,   205,    -1,    23,   177,   208,    -1,    24,    -1,
     207,    -1,   207,   208,    -1,     7,   210,   177,   331,    -1,
      -1,     8,   212,   177,   330,    -1,    -1,    11,   177,    34,
      -1,    11,   177,    12,    -1,    11,   177,    56,    -1,   232,
      -1,   215,    -1,   218,    -1,   241,    -1,   193,    -1,    56,
     178,    59,   177,   112,    -1,    56,   178,    58,   177,   112,
      -1,    -1,    51,   177,   217,   221,    -1,    -1,    52,   177,
     219,   221,    -1,    53,    -1,   220,    -1,   220,   221,    -1,
      54,   177,   120,    -1,    54,   177,   121,    -1,    55,   177,
     112,    -1,   224,    -1,   225,    -1,   226,    -1,    47,   177,
     227,    -1,    48,   177,   227,    -1,    49,   177,   227,    -1,
      45,    -1,    26,   177,   112,    -1,    27,   177,   112,    -1,
      28,   177,   112,    -1,    29,   177,   112,    -1,    30,   177,
     112,    -1,    31,   177,   112,    -1,    13,   177,   112,    -1,
     234,    -1,   235,    -1,    95,   177,    50,    -1,    96,   177,
      50,    -1,    97,   177,   120,    -1,    97,   177,   121,    -1,
     119,   177,   120,    -1,   119,   177,   121,    -1,    40,   177,
     240,    -1,    41,    -1,    42,    -1,   239,    -1,   239,   240,
      -1,    14,   177,   242,    -1,    78,    -1,    76,    -1,    77,
      -1,    15,   178,    17,   178,    77,   177,   112,    -1,    15,
     178,    18,   178,    77,   177,   112,    -1,    15,   178,    17,
     178,    76,   177,   112,    -1,    15,   178,    18,   178,    76,
     177,   112,    -1,    16,   178,    17,   178,    77,   177,   112,
      -1,    16,   178,    18,   178,    77,   177,   112,    -1,    19,
     177,   246,    -1,    20,    -1,    21,    -1,    22,    -1,   245,
      -1,   245,   246,    -1,     9,   177,    10,    -1,    32,   177,
     249,    -1,   254,    -1,   254,   249,    -1,    -1,    32,   177,
     251,   249,    -1,    -1,    33,   177,   253,   249,    -1,    34,
      -1,    35,    -1,    36,    -1,    37,    -1,    38,    -1,    39,
      -1,     6,   293,   175,   257,   300,   257,   176,    -1,   261,
      -1,   304,    -1,   297,    -1,   301,    -1,    -1,   256,   257,
      -1,   293,   175,   260,   300,   260,   176,    -1,   261,    -1,
     304,    -1,   294,    -1,   348,    -1,   297,    -1,   197,    -1,
     301,    -1,    -1,   259,   260,    -1,   248,    -1,   290,    -1,
     316,    -1,   305,    -1,   308,    -1,   309,    -1,   310,    -1,
     311,    -1,   312,    -1,   267,    -1,   274,    -1,   263,    -1,
     262,    -1,   264,    -1,   268,    -1,   269,    -1,   270,    -1,
     271,    -1,   272,    -1,   273,    -1,   275,    -1,   276,    -1,
     277,    -1,   281,    -1,   278,    -1,   282,    -1,   279,    -1,
     283,    -1,   280,    -1,   284,    -1,   265,    -1,   266,    -1,
     287,    -1,   285,    -1,   286,    -1,   288,    -1,   289,    -1,
     200,    -1,   203,    -1,   193,    -1,   315,    -1,   302,    -1,
     163,   177,   112,    -1,   163,   177,   179,   112,    -1,   154,
     177,   155,    -1,   164,   177,   112,    -1,   146,   177,   152,
      -1,   147,   177,   153,    -1,   123,   177,   124,    -1,   128,
     177,   129,    -1,   130,   177,   129,    -1,   131,   177,   129,
      -1,   148,   177,   112,    -1,   149,   177,   112,    -1,   125,
     177,   120,    -1,   125,   177,   121,    -1,   165,   177,   120,
      -1,   165,   177,   121,    -1,   126,   177,   120,    -1,   126,
     177,   121,    -1,   127,   177,   120,    -1,   127,   177,   121,
      -1,   138,   177,   150,    -1,   139,   177,   150,    -1,   140,
     177,   150,    -1,   141,   177,   150,    -1,   142,   177,   151,
      -1,   143,   177,   151,    -1,   144,   177,   151,    -1,   145,
     177,   151,    -1,   136,   177,   135,    -1,   137,   177,   135,
      -1,   134,   177,   135,    -1,   132,   177,   133,    -1,   160,
     177,   161,    -1,    43,   177,   292,    -1,    44,    -1,   291,
      -1,   291,   292,    -1,    60,    -1,    61,    -1,    86,   177,
     295,    -1,   296,    -1,   296,   295,    -1,    87,    -1,    88,
      -1,    89,    -1,    90,    -1,    91,    -1,    75,   177,   298,
      -1,   299,    -1,   299,   298,    -1,    76,    -1,    77,    -1,
     317,   318,    -1,   115,   319,   320,    -1,   115,   319,    -1,
     115,   320,    -1,   303,    -1,   117,   177,   112,    -1,   116,
     177,   112,    -1,    99,   177,   307,    -1,   100,    -1,   101,
      -1,   102,    -1,   103,    -1,   104,    -1,   306,    -1,   306,
     307,    -1,    62,   177,   111,    -1,    64,   177,    63,    -1,
      65,   177,    73,    -1,    66,   177,    74,    -1,    67,   177,
     314,    -1,    68,    -1,    69,    -1,    70,    -1,    71,    -1,
      72,    -1,   313,    -1,   313,   314,    -1,   122,   328,   177,
     331,    -1,    94,   177,    93,    -1,   324,   177,   331,    -1,
     325,   177,   331,    -1,   326,   177,   331,    -1,   327,   177,
     331,    -1,   329,   177,   333,    -1,   294,    -1,   290,    -1,
     206,    -1,   297,    -1,   310,    -1,   311,    -1,   312,    -1,
     197,    -1,   248,    -1,    -1,   322,   323,    -1,   113,    -1,
     114,    -1,   113,    -1,   114,    -1,   114,    -1,    57,    -1,
     334,    -1,   336,    -1,   337,    -1,   334,   180,   335,   340,
      -1,    -1,   334,   332,   340,    -1,   336,   340,    -1,   337,
     340,    -1,   334,   341,    -1,   336,   341,    -1,   337,   341,
      -1,   339,    -1,   338,    -1,   105,    -1,   112,    -1,   105,
      -1,   106,    -1,   108,    -1,   107,    -1,   109,    -1,    -1,
     110,   177,   342,    -1,   110,   347,   342,    -1,   110,   343,
      -1,    -1,   110,   347,   342,    -1,   346,    -1,   344,    -1,
     344,   179,   345,    -1,   112,    -1,   112,    -1,   111,    -1,
      98,    -1,   118,   177,   349,   179,   350,    -1,   112,    -1,
     112,    -1,   167,   177,   173,    -1,   168,   177,   173,    -1,
     169,   177,   173,    -1,   169,   177,    -1,   170,   177,   173,
      -1,   171,   177,   173,    -1,   172,   177,   112,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   358,   358,   359,   362,   371,   374,   375,   378,   379,
     380,   383,   384,   385,   386,   390,   394,   395,   398,   399,
     401,   402,   403,   404,   405,   406,   407,   408,   409,   410,
     411,   412,   413,   414,   415,   416,   417,   418,   419,   420,
     421,   424,   425,   426,   427,   428,   429,   430,   431,   440,
     441,   442,   443,   446,   451,   461,   489,   492,   495,   498,
     501,   504,   507,   508,   511,   514,   522,   523,   526,   529,
     537,   538,   541,   544,   549,   550,   553,   564,   581,   588,
     597,   601,   604,   614,   615,   616,   617,   618,   621,   631,
     643,   643,   646,   646,   649,   698,   699,   702,   708,   711,
     718,   719,   720,   723,   735,   747,   762,   772,   778,   782,
     789,   797,   803,   812,   820,   821,   824,   837,   850,   858,
     868,   872,   879,   882,   886,   892,   893,   896,   899,   902,
     909,   914,   919,   923,   927,   932,   936,   946,   949,   953,
     956,   962,   963,   966,   981,   984,   985,   988,   988,  1001,
    1001,  1010,  1013,  1020,  1023,  1030,  1037,  1049,  1085,  1086,
    1091,  1096,  1103,  1104,  1107,  1125,  1126,  1131,  1132,  1133,
    1134,  1135,  1142,  1143,  1146,  1147,  1148,  1149,  1150,  1151,
    1152,  1153,  1154,  1155,  1156,  1157,  1158,  1159,  1160,  1161,
    1162,  1163,  1164,  1165,  1166,  1167,  1168,  1169,  1170,  1171,
    1172,  1173,  1174,  1175,  1176,  1177,  1178,  1179,  1180,  1181,
    1182,  1183,  1184,  1185,  1186,  1191,  1198,  1203,  1212,  1225,
    1236,  1249,  1262,  1274,  1286,  1298,  1310,  1321,  1332,  1337,
    1346,  1351,  1360,  1365,  1374,  1379,  1388,  1401,  1414,  1427,
    1440,  1453,  1466,  1479,  1492,  1504,  1516,  1528,  1540,  1555,
    1558,  1567,  1568,  1572,  1577,  1584,  1587,  1588,  1591,  1594,
    1597,  1603,  1607,  1613,  1616,  1617,  1620,  1623,  1629,  1632,
    1633,  1634,  1637,  1640,  1662,  1685,  1688,  1692,  1695,  1698,
    1701,  1707,  1708,  1712,  1723,  1735,  1746,  1761,  1764,  1770,
    1773,  1776,  1779,  1787,  1788,  1791,  1794,  1821,  1825,  1828,
    1831,  1834,  1837,  1838,  1839,  1840,  1841,  1842,  1843,  1844,
    1845,  1848,  1849,  1852,  1857,  1868,  1873,  1878,  1886,  1891,
    1892,  1893,  1897,  1898,  1898,  1904,  1905,  1908,  1909,  1910,
    1911,  1912,  1916,  1925,  1932,  1938,  1948,  1959,  1970,  1981,
    1982,  1983,  1984,  1987,  1988,  1991,  1992,  1995,  2003,  2010,
    2017,  2048,  2053,  2056,  2064,  2077,  2085,  2093,  2099,  2106,
    2114,  2122
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "SERVERCONFIG", "CLIENTCONFIG",
  "DEPRECATED", "CLIENTRULE", "INTERNAL", "EXTERNAL", "REALM", "REALNAME",
  "EXTERNAL_ROTATION", "SAMESAME", "DEBUGGING", "RESOLVEPROTOCOL",
  "SOCKET", "CLIENTSIDE_SOCKET", "SNDBUF", "RCVBUF", "SRCHOST",
  "NODNSMISMATCH", "NODNSUNKNOWN", "CHECKREPLYAUTH", "EXTENSION", "BIND",
  "PRIVILEGED", "IOTIMEOUT", "IOTIMEOUT_TCP", "IOTIMEOUT_UDP",
  "NEGOTIATETIMEOUT", "CONNECTTIMEOUT", "TCP_FIN_WAIT", "METHOD",
  "CLIENTMETHOD", "NONE", "GSSAPI", "UNAME", "RFC931", "PAM", "BSDAUTH",
  "COMPATIBILITY", "SAMEPORT", "DRAFT_5_05", "CLIENTCOMPATIBILITY",
  "NECGSSAPI", "USERNAME", "GROUPNAME", "USER_PRIVILEGED",
  "USER_UNPRIVILEGED", "USER_LIBWRAP", "LIBWRAP_FILE", "ERRORLOG",
  "LOGOUTPUT", "LOGFILE", "CHILD_MAXIDLE", "CHILD_MAXREQUESTS", "ROUTE",
  "VIA", "BADROUTE_EXPIRE", "MAXFAIL", "VERDICT_BLOCK", "VERDICT_PASS",
  "PAMSERVICENAME", "BSDAUTHSTYLENAME", "BSDAUTHSTYLE", "GSSAPISERVICE",
  "GSSAPIKEYTAB", "GSSAPIENCTYPE", "GSSAPIENC_ANY", "GSSAPIENC_CLEAR",
  "GSSAPIENC_INTEGRITY", "GSSAPIENC_CONFIDENTIALITY",
  "GSSAPIENC_PERMESSAGE", "GSSAPISERVICENAME", "GSSAPIKEYTABNAME",
  "PROTOCOL", "PROTOCOL_TCP", "PROTOCOL_UDP", "PROTOCOL_FAKE",
  "PROXYPROTOCOL", "PROXYPROTOCOL_SOCKS_V4", "PROXYPROTOCOL_SOCKS_V5",
  "PROXYPROTOCOL_HTTP", "PROXYPROTOCOL_UPNP", "USER", "GROUP", "COMMAND",
  "COMMAND_BIND", "COMMAND_CONNECT", "COMMAND_UDPASSOCIATE",
  "COMMAND_BINDREPLY", "COMMAND_UDPREPLY", "ACTION", "LINE",
  "LIBWRAPSTART", "LIBWRAP_ALLOW", "LIBWRAP_DENY", "LIBWRAP_HOSTS_ACCESS",
  "OPERATOR", "SOCKS_LOG", "SOCKS_LOG_CONNECT", "SOCKS_LOG_DATA",
  "SOCKS_LOG_DISCONNECT", "SOCKS_LOG_ERROR", "SOCKS_LOG_IOOPERATION",
  "IPADDRESS", "DOMAINNAME", "DIRECT", "IFNAME", "URL", "PORT",
  "SERVICENAME", "NUMBER", "FROM", "TO", "REDIRECT", "BANDWIDTH",
  "MAXSESSIONS", "UDPPORTRANGE", "UDPCONNECTDST", "YES", "NO", "BOUNCE",
  "LDAPURL", "LDAP_URL", "LDAPSSL", "LDAPCERTCHECK", "LDAPKEEPREALM",
  "LDAPBASEDN", "LDAP_BASEDN", "LDAPBASEDN_HEX", "LDAPBASEDN_HEX_ALL",
  "LDAPSERVER", "LDAPSERVER_NAME", "LDAPGROUP", "LDAPGROUP_NAME",
  "LDAPGROUP_HEX", "LDAPGROUP_HEX_ALL", "LDAPFILTER", "LDAPFILTER_AD",
  "LDAPFILTER_HEX", "LDAPFILTER_AD_HEX", "LDAPATTRIBUTE",
  "LDAPATTRIBUTE_AD", "LDAPATTRIBUTE_HEX", "LDAPATTRIBUTE_AD_HEX",
  "LDAPCERTFILE", "LDAPCERTPATH", "LDAPPORT", "LDAPPORTSSL", "LDAP_FILTER",
  "LDAP_ATTRIBUTE", "LDAP_CERTFILE", "LDAP_CERTPATH", "LDAPDOMAIN",
  "LDAP_DOMAIN", "LDAPTIMEOUT", "LDAPCACHE", "LDAPCACHEPOS",
  "LDAPCACHENEG", "LDAPKEYTAB", "LDAPKEYTABNAME", "LDAPDEADTIME",
  "LDAPDEBUG", "LDAPDEPTH", "LDAPAUTO", "LDAPSEARCHTIME", "AUTHSERVER",
  "AUTHUSERNAME", "AUTHPASSWORD", "AUTHDATABASE", "AUTHTABLE", "AUTHDELAY",
  "AUTH_STRING", "'\\n'", "'{'", "'}'", "':'", "'.'", "'-'", "'/'",
  "$accept", "configtype", "serverinit", "serverline", "rulesorroutes",
  "ruleorroute", "clientline", "clientinit", "clientconfig",
  "serverconfigs", "serverconfig", "serveroption", "timeout", "deprecated",
  "route", "routeinit", "proxyprotocol", "proxyprotocolname",
  "proxyprotocols", "user", "username", "usernames", "group", "groupname",
  "groupnames", "extension", "extensionname", "extensions", "internal",
  "internalinit", "external", "externalinit", "external_rotation",
  "clientoption", "global_routeoption", "errorlog", "$@1", "logoutput",
  "$@2", "logoutputdevice", "logoutputdevices", "childstate", "userids",
  "user_privileged", "user_unprivileged", "user_libwrap", "userid",
  "iotimeout", "negotiatetimeout", "connecttimeout", "tcp_fin_timeout",
  "debuging", "libwrapfiles", "libwrap_allowfile", "libwrap_denyfile",
  "libwrap_hosts_access", "udpconnectdst", "compatibility",
  "compatibilityname", "compatibilitynames", "resolveprotocol",
  "resolveprotocolname", "socket", "srchost", "srchostoption",
  "srchostoptions", "realm", "authmethod", "authmethods",
  "global_authmethod", "$@3", "global_clientauthmethod", "$@4",
  "authmethodname", "clientrule", "clientruleoption", "clientruleoptions",
  "rule", "ruleoption", "ruleoptions", "option", "ldapdebug", "ldapdomain",
  "ldapdepth", "ldapcertfile", "ldapcertpath", "lurl", "lbasedn",
  "lbasedn_hex", "lbasedn_hex_all", "ldapport", "ldapportssl", "ldapssl",
  "ldapauto", "ldapcertcheck", "ldapkeeprealm", "ldapfilter",
  "ldapfilter_ad", "ldapfilter_hex", "ldapfilter_ad_hex", "ldapattribute",
  "ldapattribute_ad", "ldapattribute_hex", "ldapattribute_ad_hex",
  "lgroup_hex", "lgroup_hex_all", "lgroup", "lserver", "ldapkeytab",
  "clientcompatibility", "clientcompatibilityname",
  "clientcompatibilitynames", "verdict", "command", "commands",
  "commandname", "protocol", "protocols", "protocolname", "fromto",
  "redirect", "session", "maxsessions", "bandwidth", "log", "logname",
  "logs", "pamservicename", "bsdauthstylename", "gssapiservicename",
  "gssapikeytab", "gssapienctype", "gssapienctypename", "gssapienctypes",
  "bounce", "libwrap", "srcaddress", "dstaddress", "rdr_fromaddress",
  "rdr_toaddress", "gateway", "routeoption", "routeoptions", "from", "to",
  "rdr_from", "rdr_to", "bounce_to", "via", "externaladdress", "address",
  "$@5", "gwaddress", "ipaddress", "netmask", "domain", "ifname", "direct",
  "url", "port", "gwport", "portnumber", "portrange", "portstart",
  "portend", "portservice", "portoperator", "udpportrange",
  "udpportrange_start", "udpportrange_end", "authserver", "authusername",
  "authpassword", "authdatabase", "authtable", "authdelay", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   354,
     355,   356,   357,   358,   359,   360,   361,   362,   363,   364,
     365,   366,   367,   368,   369,   370,   371,   372,   373,   374,
     375,   376,   377,   378,   379,   380,   381,   382,   383,   384,
     385,   386,   387,   388,   389,   390,   391,   392,   393,   394,
     395,   396,   397,   398,   399,   400,   401,   402,   403,   404,
     405,   406,   407,   408,   409,   410,   411,   412,   413,   414,
     415,   416,   417,   418,   419,   420,   421,   422,   423,   424,
     425,   426,   427,   428,    10,   123,   125,    58,    46,    45,
      47
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint16 yyr1[] =
{
       0,   181,   182,   182,   183,   184,   185,   185,   186,   186,
     186,   187,   187,   187,   187,   188,   189,   189,   190,   190,
     191,   191,   191,   191,   191,   191,   191,   191,   191,   191,
     191,   191,   191,   191,   191,   191,   191,   191,   191,   191,
     191,   192,   192,   192,   192,   192,   192,   192,   192,   193,
     193,   193,   193,   194,   195,   196,   197,   198,   198,   198,
     198,   198,   199,   199,   200,   201,   202,   202,   203,   204,
     205,   205,   206,   207,   208,   208,   209,   210,   211,   212,
     213,   213,   213,   214,   214,   214,   214,   214,   215,   215,
     217,   216,   219,   218,   220,   221,   221,   222,   222,   222,
     223,   223,   223,   224,   225,   226,   227,   228,   228,   228,
     229,   230,   231,   232,   233,   233,   234,   235,   236,   236,
     237,   237,   238,   239,   239,   240,   240,   241,   242,   242,
     242,   243,   243,   243,   243,   243,   243,   244,   245,   245,
     245,   246,   246,   247,   248,   249,   249,   251,   250,   253,
     252,   254,   254,   254,   254,   254,   254,   255,   256,   256,
     256,   256,   257,   257,   258,   259,   259,   259,   259,   259,
     259,   259,   260,   260,   261,   261,   261,   261,   261,   261,
     261,   261,   261,   261,   261,   261,   261,   261,   261,   261,
     261,   261,   261,   261,   261,   261,   261,   261,   261,   261,
     261,   261,   261,   261,   261,   261,   261,   261,   261,   261,
     261,   261,   261,   261,   261,   261,   262,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   273,
     274,   274,   275,   275,   276,   276,   277,   278,   279,   280,
     281,   282,   283,   284,   285,   286,   287,   288,   289,   290,
     291,   292,   292,   293,   293,   294,   295,   295,   296,   296,
     296,   296,   296,   297,   298,   298,   299,   299,   300,   301,
     301,   301,   302,   303,   304,   305,   306,   306,   306,   306,
     306,   307,   307,   308,   309,   310,   311,   312,   313,   313,
     313,   313,   313,   314,   314,   315,   316,   317,   318,   319,
     320,   321,   322,   322,   322,   322,   322,   322,   322,   322,
     322,   323,   323,   324,   325,   326,   327,   328,   329,   330,
     330,   330,   331,   332,   331,   331,   331,   333,   333,   333,
     333,   333,   334,   335,   335,   336,   337,   338,   339,   340,
     340,   340,   340,   341,   341,   342,   342,   343,   344,   345,
     346,   347,   348,   349,   350,   351,   352,   353,   353,   354,
     355,   356
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     2,     2,     1,     2,     0,     2,     1,     1,
       1,     0,     2,     2,     2,     1,     1,     1,     1,     2,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     8,     0,     3,     1,     1,     1,
       1,     1,     1,     2,     3,     1,     1,     2,     3,     1,
       1,     2,     3,     1,     1,     2,     4,     0,     4,     0,
       3,     3,     3,     1,     1,     1,     1,     1,     5,     5,
       0,     4,     0,     4,     1,     1,     2,     3,     3,     3,
       1,     1,     1,     3,     3,     3,     1,     3,     3,     3,
       3,     3,     3,     3,     1,     1,     3,     3,     3,     3,
       3,     3,     3,     1,     1,     1,     2,     3,     1,     1,
       1,     7,     7,     7,     7,     7,     7,     3,     1,     1,
       1,     1,     2,     3,     3,     1,     2,     0,     4,     0,
       4,     1,     1,     1,     1,     1,     1,     7,     1,     1,
       1,     1,     0,     2,     6,     1,     1,     1,     1,     1,
       1,     1,     0,     2,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     3,     4,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       1,     1,     2,     1,     1,     3,     1,     2,     1,     1,
       1,     1,     1,     3,     1,     2,     1,     1,     2,     3,
       2,     2,     1,     3,     3,     3,     1,     1,     1,     1,
       1,     1,     2,     3,     3,     3,     3,     3,     1,     1,
       1,     1,     1,     1,     2,     4,     3,     3,     3,     3,
       3,     3,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     0,     2,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     4,     0,     3,     2,     2,     2,     2,     2,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     0,
       3,     3,     2,     0,     3,     1,     1,     3,     1,     1,
       1,     1,     5,     1,     1,     3,     3,     3,     2,     3,
       3,     3
};

/* YYDEFACT[STATE-NAME] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint16 yydefact[] =
{
       0,     4,    15,     0,     0,    11,     1,    53,    77,    79,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     2,     6,    18,    28,    48,
      22,    42,    23,    24,    25,    43,    26,    27,    30,    29,
     100,   101,   102,    50,    51,    49,    52,    31,    32,   114,
     115,    33,    34,    41,    44,    46,    47,    45,    20,    21,
      35,    36,    37,    38,    39,    40,     3,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   147,   149,     0,     0,     0,     0,    90,
      92,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     358,     0,     0,     0,     0,    55,   253,   254,     5,     6,
      19,    10,     8,     9,     0,    12,    13,    87,    17,    14,
      16,    84,    85,    83,    86,     0,     0,   143,    81,    80,
      82,   113,   129,   130,   128,   127,     0,     0,     0,     0,
     138,   139,   140,   141,   137,    73,    74,    72,   107,   108,
     109,   110,   111,   112,     0,     0,   123,   124,   125,   122,
     106,   103,   104,   105,     0,     0,    97,    98,    99,     0,
       0,   116,   117,   118,   119,   120,   121,   355,   356,   357,
     359,   360,   361,     0,     0,    55,     7,   172,   332,   335,
     336,    76,   323,   339,   339,    78,   319,   320,   321,     0,
       0,     0,     0,   142,    75,   151,   152,   153,   154,   155,
     156,   148,   145,   150,   126,    94,    95,    91,    93,     0,
       0,   162,   311,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     213,   170,   211,   212,   174,   172,     0,   165,   186,   185,
     187,   204,   205,   183,   188,   189,   190,   191,   192,   193,
     184,   194,   195,   196,   198,   200,   202,   197,   199,   201,
     203,   207,   208,   206,   209,   210,   175,   167,   169,   171,
     215,   272,   166,   177,   178,   179,   180,   181,   182,   214,
     176,   168,     0,   339,     0,   325,   326,     0,     0,     0,
       0,     0,     0,   146,    96,    89,    88,   162,     0,   158,
     160,   161,   159,   309,   304,   310,   303,   302,   305,   306,
     307,   308,   311,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   315,   316,
     270,   271,     0,     0,     0,     0,     0,   317,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   173,   313,   172,
       0,     0,   334,   333,   339,   324,   351,   348,     0,   342,
       0,     0,     0,     0,     0,     0,     0,     0,   163,   162,
     312,     0,   144,   250,   251,   249,   283,   284,   285,   286,
     288,   289,   290,   291,   292,   293,   287,   266,   267,   263,
     264,    57,    58,    59,    60,    61,    62,    56,    65,    66,
      64,    69,    70,    68,   258,   259,   260,   261,   262,   255,
     256,   296,   276,   277,   278,   279,   280,   281,   275,   269,
       0,     0,   274,   273,   353,     0,     0,   222,   228,   229,
     232,   233,   234,   235,   223,   224,   225,   247,   246,   244,
     245,   236,   237,   238,   239,   240,   241,   242,   243,   220,
     221,   226,   227,   218,   248,   216,     0,   219,   230,   231,
       0,   314,   268,     0,     0,   322,   350,   340,   346,   345,
       0,   341,   133,   131,   134,   132,   135,   136,     0,   318,
     311,     0,   252,   294,   265,    63,    67,    71,   257,   282,
     299,   300,     0,   295,   217,   164,     0,   297,   349,   347,
     157,     0,     0,   354,   352,   298,    54,   337,   338,   301,
     343,   343,   343,   331,   330,     0,   327,   328,   329,     0,
     344
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     3,     4,    45,   128,   129,    86,     5,   136,    46,
      47,    48,   290,    50,   131,   204,   291,   476,   477,   292,
     479,   480,   293,   482,   483,   364,   166,   167,    52,    87,
      53,    88,    54,   140,    55,    56,   184,    57,   185,   236,
     237,    58,    59,    60,    61,    62,   181,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,   178,   179,
      74,   155,    75,    76,   163,   164,    77,   294,   231,    78,
     174,    79,   175,   232,   132,   357,   358,   133,   295,   296,
     297,   298,   299,   300,   301,   302,   303,   304,   305,   306,
     307,   308,   309,   310,   311,   312,   313,   314,   315,   316,
     317,   318,   319,   320,   321,   322,   323,   324,   325,   326,
     454,   455,   134,   327,   489,   490,   328,   469,   470,   429,
     329,   330,   331,   332,   333,   497,   498,   334,   335,   336,
     337,   338,   465,   466,   339,   340,   430,   542,   390,   391,
     560,   372,   373,   431,   543,   392,   393,   398,   561,   215,
     211,   343,   589,   212,   434,   213,   214,   593,   594,   345,
     596,   547,   439,   548,   579,   549,   441,   341,   505,   584,
      80,    81,    82,    83,    84,    85
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -468
static const yytype_int16 yypact[] =
{
      48,  -468,  -468,    36,   236,  -468,  -468,  -468,  -468,  -468,
    -129,  -116,  -110,  -108,  -102,   -66,   -74,   -51,   -47,   -33,
     -25,   -23,   -17,   -13,     4,    15,    24,    38,    45,    50,
      51,    52,    55,    56,   -59,    58,    63,    65,    71,    77,
      80,    81,    94,    95,    96,  -468,   142,  -468,  -468,  -468,
    -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,
    -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,
    -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,
    -468,  -468,  -468,  -468,  -468,  -468,    14,    97,    98,   185,
      19,   118,     2,    66,    76,    68,   207,   124,   134,   141,
     144,   148,   158,  -468,  -468,    54,   232,   232,   232,  -468,
    -468,   -22,   166,    42,   229,   230,   -10,     7,   108,   109,
     113,   116,   120,   182,    73,   -59,  -468,  -468,  -468,    26,
    -468,  -468,  -468,  -468,   121,  -468,  -468,  -468,  -468,  -468,
    -468,  -468,  -468,  -468,  -468,   -43,   -43,  -468,  -468,  -468,
    -468,  -468,  -468,  -468,  -468,  -468,   117,   119,   122,   123,
    -468,  -468,  -468,    68,  -468,  -468,   207,  -468,  -468,  -468,
    -468,  -468,  -468,  -468,    21,    21,  -468,  -468,    54,  -468,
    -468,  -468,  -468,  -468,   245,   245,  -468,  -468,  -468,   125,
     126,  -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,
    -468,  -468,  -468,   129,   130,  -468,  -468,   334,  -468,  -468,
    -468,  -468,   127,   196,   196,  -468,  -468,  -468,  -468,    69,
      86,   231,   238,  -468,  -468,  -468,  -468,  -468,  -468,  -468,
    -468,  -468,    21,  -468,  -468,  -468,   245,  -468,  -468,   204,
     205,   474,     6,   143,   145,   157,   159,   160,   161,   162,
     163,   164,   165,   167,   168,   169,   170,    53,   171,   172,
     173,   221,   174,   175,   176,   177,   179,   180,   181,   190,
     191,   192,   193,   194,   195,   197,   198,   199,   201,   202,
     203,   206,   208,   209,   210,   211,   212,   213,   214,   215,
    -468,  -468,  -468,  -468,  -468,   334,   246,  -468,  -468,  -468,
    -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,
    -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,
    -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,
    -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,
    -468,  -468,   -58,   196,   -75,  -468,  -468,   217,   218,   220,
     225,   233,   234,  -468,  -468,  -468,  -468,   474,   246,  -468,
    -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,
    -468,  -468,     6,   246,    21,   274,   262,   256,   248,   269,
      46,   100,    25,   336,   338,    34,   289,    83,  -468,  -468,
     298,  -468,   237,   239,   303,   305,   309,  -468,   247,   299,
      59,    79,    84,   293,   296,   297,   294,   295,   300,   304,
     279,   281,   282,   284,   290,   291,   292,   302,   288,   301,
     332,   333,   308,   285,   -88,   335,    93,  -468,  -468,   334,
     341,   271,  -468,  -468,   196,  -468,  -468,  -468,   106,  -468,
     306,   106,   346,   355,   357,   372,   374,   375,  -468,   474,
    -468,   432,  -468,  -468,   274,  -468,  -468,  -468,  -468,  -468,
    -468,  -468,  -468,  -468,  -468,    46,  -468,  -468,  -468,  -468,
     100,  -468,  -468,  -468,  -468,  -468,    25,  -468,  -468,   336,
    -468,  -468,   338,  -468,  -468,  -468,  -468,  -468,  -468,  -468,
      34,  -468,  -468,  -468,  -468,  -468,  -468,    83,  -468,  -468,
     -43,   -43,  -468,  -468,  -468,   311,   -43,  -468,  -468,  -468,
    -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,
    -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,
    -468,  -468,  -468,  -468,  -468,  -468,   379,  -468,  -468,  -468,
     316,  -468,  -468,   318,   -43,  -468,  -468,  -468,  -468,  -468,
     381,  -468,  -468,  -468,  -468,  -468,  -468,  -468,   320,  -468,
       6,   330,  -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,
    -468,  -468,   396,  -468,  -468,  -468,   -43,  -468,  -468,  -468,
    -468,   337,   102,  -468,  -468,  -468,  -468,  -468,  -468,  -468,
     399,   399,   399,  -468,  -468,   412,  -468,  -468,  -468,   106,
    -468
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -468,  -468,  -468,  -468,   382,  -468,  -468,  -468,  -468,  -468,
     466,  -468,    18,   -83,   428,  -468,  -236,  -468,    39,  -468,
    -468,    37,  -468,  -468,    40,    22,  -468,   352,  -468,  -468,
    -468,  -468,  -468,  -468,   433,  -468,  -468,   434,  -468,  -468,
    -139,  -468,  -468,  -468,  -468,  -468,   112,  -468,  -468,  -468,
    -468,   435,  -468,  -468,  -468,  -468,  -468,  -468,  -468,   345,
     438,  -468,  -468,  -468,  -468,   362,  -468,  -235,  -158,  -468,
    -468,  -468,  -468,  -468,  -468,  -468,  -336,  -468,  -468,  -270,
    -228,  -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,
    -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,
    -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,  -468,  -234,
    -468,    72,   403,  -233,    41,  -468,  -237,    60,  -468,  -323,
    -226,  -468,  -468,  -225,  -468,  -468,    31,  -468,  -468,  -232,
    -231,  -230,  -468,    64,  -468,  -468,  -468,  -468,  -468,   147,
    -468,  -468,  -354,  -468,  -468,  -468,  -468,  -468,  -468,  -468,
    -467,  -468,  -468,  -146,  -468,  -145,  -144,  -468,  -468,  -200,
    -366,  -421,  -468,   188,  -468,  -468,   -62,  -468,  -468,  -468,
    -468,  -468,  -468,  -468,  -468,  -468
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -1
static const yytype_uint16 yytable[] =
{
     216,   217,   218,   138,   360,   368,   363,   365,   366,   367,
     369,   370,   371,   359,   346,   361,   362,   233,   450,     7,
     551,   448,    49,   436,   535,   427,    51,    12,    13,    17,
       7,   148,   124,   570,   571,   449,     6,   437,   243,   573,
      18,    19,    20,    21,    22,    23,   238,   432,    89,   244,
     451,     1,     2,   149,   433,   225,   226,   227,   228,   229,
     230,    90,   208,   209,    49,   210,    31,    91,    51,    92,
     125,   247,   248,   249,   353,   150,    93,   577,   152,   153,
     154,   250,   205,   156,   157,   251,   126,   127,   160,   161,
     162,   536,   254,   158,   159,   176,   177,   354,   186,   187,
     189,   190,   438,    95,   137,   471,   472,   473,   474,   585,
     193,   194,    94,   558,   460,   461,   462,   463,   464,   113,
     360,   484,   485,   486,   487,   488,    96,   195,   196,   359,
      97,   361,   362,   126,   127,   368,   363,   365,   366,   367,
     369,   370,   371,   435,    98,   347,   348,     7,   124,     8,
       9,    10,    99,    11,   100,    12,    13,    14,    15,   540,
     101,    16,   349,   350,   102,    17,   388,   389,    18,    19,
      20,    21,    22,    23,    24,    25,   467,   468,   600,   508,
     509,   103,    26,   492,   493,   494,   495,   496,   135,    27,
      28,    29,   104,    30,    31,   147,    32,    33,   125,   510,
     511,   105,   126,   127,   512,   513,   581,   208,   209,   587,
     210,   588,   360,   538,   539,   106,   452,   546,   437,   182,
     183,   359,   107,   361,   362,   597,   598,   108,   109,   110,
     151,   165,   111,   112,   545,   114,   168,    35,    36,    37,
     115,     7,   116,     8,     9,    10,   169,    11,   117,    12,
      13,    14,    15,   170,   118,    16,   171,   119,   120,    17,
     172,    38,    18,    19,    20,    21,    22,    23,    24,    25,
     173,   121,   122,   123,   145,   146,    26,   180,   188,   191,
     192,   197,   198,    27,    28,    29,   199,    30,    31,   200,
      32,    33,    34,   201,   202,   219,   207,   220,   235,   475,
     221,   222,   239,   240,   241,   242,   344,   342,   351,    39,
      40,    41,    42,    43,    44,   352,   355,   356,   453,   457,
     374,   458,   375,   368,   363,   365,   366,   367,   369,   370,
     371,    35,    36,    37,   376,   397,   377,   378,   379,   380,
     381,   382,   383,   459,   384,   385,   386,   387,   394,   395,
     396,   399,   400,   401,   402,    38,   403,   404,   405,   428,
      18,    19,    20,    21,    22,    23,   243,   406,   407,   408,
     409,   410,   411,   456,   412,   413,   414,   244,   415,   416,
     417,   478,   491,   418,   481,   419,   420,   421,   422,   423,
     424,   425,   426,   475,   442,   443,   245,   444,   246,   247,
     248,   249,   445,    39,    40,    41,    42,    43,    44,   250,
     446,   447,   389,   251,   500,   502,   501,   503,   252,   253,
     254,   504,   514,   507,   506,   515,   516,   517,   255,   521,
     518,   522,   523,   256,   524,   519,   590,   591,   592,   520,
     529,   525,   526,   527,   531,   532,   534,   537,   544,   257,
     258,   259,   260,   528,   530,   541,   261,   262,   552,   263,
     264,   265,   266,   533,   267,   268,   269,   553,   270,   554,
     271,   272,   273,   274,   275,   276,   277,   278,   279,   280,
     281,   282,   283,   284,   555,   550,   556,   557,   285,   559,
     572,   574,   575,   578,   286,   576,   580,   287,   288,   289,
      18,    19,    20,    21,    22,    23,   243,   582,   583,   595,
     436,   206,   130,   586,   139,   565,   566,   244,   224,   141,
     142,   143,   567,   234,   144,   223,   562,   203,   569,   563,
     564,   568,   440,   599,     0,     0,   245,   499,   246,   247,
     248,   249,     0,     0,     0,     0,     0,     0,     0,   250,
       0,     0,     0,     0,     0,     0,     0,     0,   252,   253,
       0,     0,     0,     0,     0,     0,     0,     0,   255,     0,
       0,     0,     0,   256,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   257,
     258,   259,     0,     0,     0,     0,   261,   262,     0,   263,
     264,   265,   266,     0,   267,   268,   269,     0,   270,     0,
     271,   272,   273,   274,   275,   276,   277,   278,   279,   280,
     281,   282,   283,   284,     0,     0,     0,     0,   285,     0,
       0,     0,     0,     0,   286,     0,     0,   287,   288,   289
};

#define yypact_value_is_default(yystate) \
  ((yystate) == (-468))

#define yytable_value_is_error(yytable_value) \
  YYID (0)

static const yytype_int16 yycheck[] =
{
     146,   146,   146,    86,   241,   242,   242,   242,   242,   242,
     242,   242,   242,   241,   214,   241,   241,   175,   372,     5,
     441,   357,     4,    98,   112,   295,     4,    13,    14,    23,
       5,    12,     6,   500,   501,   358,     0,   112,    32,   506,
      26,    27,    28,    29,    30,    31,   185,   105,   177,    43,
     373,     3,     4,    34,   112,    34,    35,    36,    37,    38,
      39,   177,   105,   106,    46,   108,    52,   177,    46,   177,
      56,    65,    66,    67,   232,    56,   178,   544,    76,    77,
      78,    75,    56,    17,    18,    79,    60,    61,    20,    21,
      22,   179,    86,    17,    18,    41,    42,   236,   120,   121,
      58,    59,   177,   177,    86,    80,    81,    82,    83,   576,
     120,   121,   178,   449,    68,    69,    70,    71,    72,   178,
     357,    87,    88,    89,    90,    91,   177,   120,   121,   357,
     177,   357,   357,    60,    61,   372,   372,   372,   372,   372,
     372,   372,   372,   343,   177,    76,    77,     5,     6,     7,
       8,     9,   177,    11,   177,    13,    14,    15,    16,   429,
     177,    19,    76,    77,   177,    23,   113,   114,    26,    27,
      28,    29,    30,    31,    32,    33,    76,    77,   599,   120,
     121,   177,    40,   100,   101,   102,   103,   104,   174,    47,
      48,    49,   177,    51,    52,    10,    54,    55,    56,   120,
     121,   177,    60,    61,   120,   121,   560,   105,   106,   107,
     108,   109,   449,   120,   121,   177,   374,   111,   112,   107,
     108,   449,   177,   449,   449,   591,   592,   177,   177,   177,
     112,    24,   177,   177,   434,   177,   112,    95,    96,    97,
     177,     5,   177,     7,     8,     9,   112,    11,   177,    13,
      14,    15,    16,   112,   177,    19,   112,   177,   177,    23,
     112,   119,    26,    27,    28,    29,    30,    31,    32,    33,
     112,   177,   177,   177,   177,   177,    40,    45,   112,    50,
      50,   173,   173,    47,    48,    49,   173,    51,    52,   173,
      54,    55,    56,   173,   112,   178,   175,   178,    53,   382,
     178,   178,   177,   177,   175,   175,   110,   180,    77,   167,
     168,   169,   170,   171,   172,    77,   112,   112,    44,    63,
     177,    73,   177,   560,   560,   560,   560,   560,   560,   560,
     560,    95,    96,    97,   177,   114,   177,   177,   177,   177,
     177,   177,   177,    74,   177,   177,   177,   177,   177,   177,
     177,   177,   177,   177,   177,   119,   177,   177,   177,   113,
      26,    27,    28,    29,    30,    31,    32,   177,   177,   177,
     177,   177,   177,   111,   177,   177,   177,    43,   177,   177,
     177,    45,    93,   177,    46,   177,   177,   177,   177,   177,
     177,   177,   177,   476,   177,   177,    62,   177,    64,    65,
      66,    67,   177,   167,   168,   169,   170,   171,   172,    75,
     177,   177,   114,    79,   177,   112,   177,   112,    84,    85,
      86,   112,   129,   124,   177,   129,   129,   133,    94,   150,
     135,   150,   150,    99,   150,   135,   582,   582,   582,   135,
     152,   151,   151,   151,   112,   112,   161,   112,   177,   115,
     116,   117,   118,   151,   153,   114,   122,   123,   112,   125,
     126,   127,   128,   155,   130,   131,   132,   112,   134,   112,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,   149,   112,   179,   112,   112,   154,    57,
     179,   112,   176,   112,   160,   177,   176,   163,   164,   165,
      26,    27,    28,    29,    30,    31,    32,   177,   112,   110,
      98,   129,    46,   176,    86,   476,   479,    43,   166,    86,
      86,    86,   482,   178,    86,   163,   454,   124,   497,   465,
     470,   490,   344,   595,    -1,    -1,    62,   390,    64,    65,
      66,    67,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    75,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    84,    85,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    94,    -1,
      -1,    -1,    -1,    99,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   115,
     116,   117,    -1,    -1,    -1,    -1,   122,   123,    -1,   125,
     126,   127,   128,    -1,   130,   131,   132,    -1,   134,    -1,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,   149,    -1,    -1,    -1,    -1,   154,    -1,
      -1,    -1,    -1,    -1,   160,    -1,    -1,   163,   164,   165
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint16 yystos[] =
{
       0,     3,     4,   182,   183,   188,     0,     5,     7,     8,
       9,    11,    13,    14,    15,    16,    19,    23,    26,    27,
      28,    29,    30,    31,    32,    33,    40,    47,    48,    49,
      51,    52,    54,    55,    56,    95,    96,    97,   119,   167,
     168,   169,   170,   171,   172,   184,   190,   191,   192,   193,
     194,   206,   209,   211,   213,   215,   216,   218,   222,   223,
     224,   225,   226,   228,   229,   230,   231,   232,   233,   234,
     235,   236,   237,   238,   241,   243,   244,   247,   250,   252,
     351,   352,   353,   354,   355,   356,   187,   210,   212,   177,
     177,   177,   177,   178,   178,   177,   177,   177,   177,   177,
     177,   177,   177,   177,   177,   177,   177,   177,   177,   177,
     177,   177,   177,   178,   177,   177,   177,   177,   177,   177,
     177,   177,   177,   177,     6,    56,    60,    61,   185,   186,
     191,   195,   255,   258,   293,   174,   189,   193,   194,   195,
     214,   215,   218,   232,   241,   177,   177,    10,    12,    34,
      56,   112,    76,    77,    78,   242,    17,    18,    17,    18,
      20,    21,    22,   245,   246,    24,   207,   208,   112,   112,
     112,   112,   112,   112,   251,   253,    41,    42,   239,   240,
      45,   227,   227,   227,   217,   219,   120,   121,   112,    58,
      59,    50,    50,   120,   121,   120,   121,   173,   173,   173,
     173,   173,   112,   293,   196,    56,   185,   175,   105,   106,
     108,   331,   334,   336,   337,   330,   334,   336,   337,   178,
     178,   178,   178,   246,   208,    34,    35,    36,    37,    38,
      39,   249,   254,   249,   240,    53,   220,   221,   221,   177,
     177,   175,   175,    32,    43,    62,    64,    65,    66,    67,
      75,    79,    84,    85,    86,    94,    99,   115,   116,   117,
     118,   122,   123,   125,   126,   127,   128,   130,   131,   132,
     134,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,   146,   147,   148,   149,   154,   160,   163,   164,   165,
     193,   197,   200,   203,   248,   259,   260,   261,   262,   263,
     264,   265,   266,   267,   268,   269,   270,   271,   272,   273,
     274,   275,   276,   277,   278,   279,   280,   281,   282,   283,
     284,   285,   286,   287,   288,   289,   290,   294,   297,   301,
     302,   303,   304,   305,   308,   309,   310,   311,   312,   315,
     316,   348,   180,   332,   110,   340,   340,    76,    77,    76,
      77,    77,    77,   249,   221,   112,   112,   256,   257,   261,
     297,   301,   304,   197,   206,   248,   290,   294,   297,   310,
     311,   312,   322,   323,   177,   177,   177,   177,   177,   177,
     177,   177,   177,   177,   177,   177,   177,   177,   113,   114,
     319,   320,   326,   327,   177,   177,   177,   114,   328,   177,
     177,   177,   177,   177,   177,   177,   177,   177,   177,   177,
     177,   177,   177,   177,   177,   177,   177,   177,   177,   177,
     177,   177,   177,   177,   177,   177,   177,   260,   113,   300,
     317,   324,   105,   112,   335,   340,    98,   112,   177,   343,
     344,   347,   177,   177,   177,   177,   177,   177,   257,   300,
     323,   300,   249,    44,   291,   292,   111,    63,    73,    74,
      68,    69,    70,    71,    72,   313,   314,    76,    77,   298,
     299,    80,    81,    82,    83,   194,   198,   199,    45,   201,
     202,    46,   204,   205,    87,    88,    89,    90,    91,   295,
     296,    93,   100,   101,   102,   103,   104,   306,   307,   320,
     177,   177,   112,   112,   112,   349,   177,   124,   120,   121,
     120,   121,   120,   121,   129,   129,   129,   133,   135,   135,
     135,   150,   150,   150,   150,   151,   151,   151,   151,   152,
     153,   112,   112,   155,   161,   112,   179,   112,   120,   121,
     260,   114,   318,   325,   177,   340,   111,   342,   344,   346,
     179,   342,   112,   112,   112,   112,   112,   112,   257,    57,
     321,   329,   292,   314,   298,   199,   202,   205,   295,   307,
     331,   331,   179,   331,   112,   176,   177,   331,   112,   345,
     176,   323,   177,   112,   350,   331,   176,   107,   109,   333,
     334,   336,   337,   338,   339,   110,   341,   341,   341,   347,
     342
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  However,
   YYFAIL appears to be in use.  Nevertheless, it is formally deprecated
   in Bison 2.4.2's NEWS entry, where a plan to phase it out is
   discussed.  */

#define YYFAIL		goto yyerrlab
#if defined YYFAIL
  /* This is here to suppress warnings from the GCC cpp's
     -Wunused-macros.  Normally we don't worry about that warning, but
     some users do, and we want to make it easy for users to remove
     YYFAIL uses, which will produce warnings from Bison 2.5.  */
#endif

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* This macro is provided for backward compatibility. */

#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
#else
static void
yy_stack_print (yybottom, yytop)
    yytype_int16 *yybottom;
    yytype_int16 *yytop;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (0, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  YYSIZE_T yysize1;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = 0;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - Assume YYFAIL is not used.  It's too flawed to consider.  See
       <http://lists.gnu.org/archive/html/bison-patches/2009-12/msg00024.html>
       for details.  YYERROR is fine as it does not invoke this
       function.
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                yysize1 = yysize + yytnamerr (0, yytname[yyx]);
                if (! (yysize <= yysize1
                       && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                  return 2;
                yysize = yysize1;
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  yysize1 = yysize + yystrlen (yyformat);
  if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
    return 2;
  yysize = yysize1;

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */
#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */


/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       `yyss': related to states.
       `yyvs': related to semantic values.

       Refer to the stacks thru separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yytoken = 0;
  yyss = yyssa;
  yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */
  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss_alloc, yyss);
	YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 4:

/* Line 1806 of yacc.c  */
#line 362 "config_parse.y"
    {
#if !SOCKS_CLIENT
      protocol  = &protocolmem;
      extension = &sockscf.extension;
#endif /* !SOCKS_CLIENT*/
   }
    break;

  case 6:

/* Line 1806 of yacc.c  */
#line 374 "config_parse.y"
    { (yyval.string) = NULL; }
    break;

  case 11:

/* Line 1806 of yacc.c  */
#line 383 "config_parse.y"
    { (yyval.string) = NULL; }
    break;

  case 15:

/* Line 1806 of yacc.c  */
#line 390 "config_parse.y"
    {
   }
    break;

  case 48:

/* Line 1806 of yacc.c  */
#line 431 "config_parse.y"
    {
#if !SOCKS_CLIENT
                  if (timeout->tcp_fin_wait == 0
                  ||  timeout->tcp_fin_wait >  timeout->tcpio)
                     timeout->tcp_fin_wait = timeout->tcpio;
#endif /* !SOCKS_CLIENT */
      }
    break;

  case 53:

/* Line 1806 of yacc.c  */
#line 446 "config_parse.y"
    {
      yyerror("given keyword, \"%s\", is deprecated", (yyvsp[(1) - (1)].string));
   }
    break;

  case 54:

/* Line 1806 of yacc.c  */
#line 451 "config_parse.y"
    {
      route.src       = src;
      route.dst       = dst;
      route.gw.addr   = gw;
      route.gw.state  = state;

      socks_addroute(&route, 1);
   }
    break;

  case 55:

/* Line 1806 of yacc.c  */
#line 461 "config_parse.y"
    {
      command             = &state.command;
      extension           = &state.extension;
      methodv             = state.methodv;
      methodc             = &state.methodc;
      protocol            = &state.protocol;
      proxyprotocol       = &state.proxyprotocol;

#if HAVE_GSSAPI
      gssapiservicename = state.gssapiservicename;
      gssapikeytab      = state.gssapikeytab;
      gssapiencryption  = &state.gssapiencryption;
#endif /* HAVE_GSSAPI */
#if HAVE_LDAP
      ldap              = &state.ldap;
#endif /* HAVE_LDAP*/

      bzero(&state, sizeof(state));
      bzero(&route, sizeof(route));
      bzero(&gw, sizeof(gw));
      bzero(&src, sizeof(src));
      bzero(&dst, sizeof(dst));
      src.atype = SOCKS_ADDR_IPV4;
      dst.atype = SOCKS_ADDR_IPV4;
   }
    break;

  case 57:

/* Line 1806 of yacc.c  */
#line 492 "config_parse.y"
    {
         proxyprotocol->socks_v4    = 1;
   }
    break;

  case 58:

/* Line 1806 of yacc.c  */
#line 495 "config_parse.y"
    {
         proxyprotocol->socks_v5    = 1;
   }
    break;

  case 59:

/* Line 1806 of yacc.c  */
#line 498 "config_parse.y"
    {
         proxyprotocol->http        = 1;
   }
    break;

  case 60:

/* Line 1806 of yacc.c  */
#line 501 "config_parse.y"
    {
         proxyprotocol->upnp        = 1;
   }
    break;

  case 65:

/* Line 1806 of yacc.c  */
#line 514 "config_parse.y"
    {
#if !SOCKS_CLIENT
      if (addlinkedname(&rule.user, (yyvsp[(1) - (1)].string)) == NULL)
         yyerror(NOMEM);
#endif /* !SOCKS_CLIENT */
   }
    break;

  case 69:

/* Line 1806 of yacc.c  */
#line 529 "config_parse.y"
    {
#if !SOCKS_CLIENT
      if (addlinkedname(&rule.group, (yyvsp[(1) - (1)].string)) == NULL)
         yyerror(NOMEM);
#endif /* !SOCKS_CLIENT */
   }
    break;

  case 73:

/* Line 1806 of yacc.c  */
#line 544 "config_parse.y"
    {
         extension->bind = 1;
   }
    break;

  case 76:

/* Line 1806 of yacc.c  */
#line 553 "config_parse.y"
    {
#if !SOCKS_CLIENT
#if BAREFOOTD
      yyerror("\"internal:\" specification is not used in %s", PACKAGE);
#endif /* BAREFOOTD */

      addinternal(ruleaddr, SOCKS_TCP);
#endif /* !SOCKS_CLIENT */
   }
    break;

  case 77:

/* Line 1806 of yacc.c  */
#line 564 "config_parse.y"
    {
#if !SOCKS_CLIENT
   static struct ruleaddr_t mem;
   struct servent   *service;

   addrinit(&mem, 0);
   bzero(protocol, sizeof(*protocol));

   /* set default port. */
   if ((service = getservbyname("socks", "tcp")) == NULL)
      *port_tcp = htons(SOCKD_PORT);
   else
      *port_tcp = (in_port_t)service->s_port;
#endif /* !SOCKS_CLIENT */
   }
    break;

  case 78:

/* Line 1806 of yacc.c  */
#line 581 "config_parse.y"
    {
#if !SOCKS_CLIENT
      addexternal(ruleaddr);
#endif /* !SOCKS_CLIENT */
   }
    break;

  case 79:

/* Line 1806 of yacc.c  */
#line 588 "config_parse.y"
    {
#if !SOCKS_CLIENT
      static struct ruleaddr_t mem;

      addrinit(&mem, 0);
#endif /* !SOCKS_CLIENT */
   }
    break;

  case 80:

/* Line 1806 of yacc.c  */
#line 597 "config_parse.y"
    {
#if !SOCKS_CLIENT
      sockscf.external.rotation = ROTATION_NONE;
   }
    break;

  case 81:

/* Line 1806 of yacc.c  */
#line 601 "config_parse.y"
    {
      sockscf.external.rotation = ROTATION_SAMESAME;
   }
    break;

  case 82:

/* Line 1806 of yacc.c  */
#line 604 "config_parse.y"
    {
#if !HAVE_ROUTE_SOURCE
      yyerror("don't have code to discover route/address source on platform");
#else /* !HAVE_ROUTE_SOURCE */
      sockscf.external.rotation = ROTATION_ROUTE;
#endif /* HAVE_ROUTE_SOURCE */
#endif /* SOCKS_SERVER */
   }
    break;

  case 88:

/* Line 1806 of yacc.c  */
#line 621 "config_parse.y"
    {
      const int value = atoi((yyvsp[(5) - (5)].string));

      if (value < 0)
         yyerror("max route fails can not be negative (%d)  Use \"0\" to "
                 "indicate routes should never be marked as bad",
                 value);

      sockscf.routeoptions.maxfail = value;
   }
    break;

  case 89:

/* Line 1806 of yacc.c  */
#line 631 "config_parse.y"
    {
      const int value = atoi((yyvsp[(5) - (5)].string));

      if (value < 0)
         yyerror("route failure expiry time can not be negative (%d).  "
                 "Use \"0\" to indicate bad route marking should never expire",
                 value);

      sockscf.routeoptions.badexpire = value;
   }
    break;

  case 90:

/* Line 1806 of yacc.c  */
#line 643 "config_parse.y"
    { add_to_errorlog = 1; }
    break;

  case 92:

/* Line 1806 of yacc.c  */
#line 646 "config_parse.y"
    { add_to_errorlog = 0; }
    break;

  case 94:

/* Line 1806 of yacc.c  */
#line 649 "config_parse.y"
    {
   int p;
#if !SOCKS_CLIENT && !HAVE_PRIVILEGES
   const struct userid_t currentuserid = sockscf.uid;;
   struct userid_t zuid;

   bzero(&zuid, sizeof(zuid));
   if (memcmp(&zuid, &sockscf.uid, sizeof(zuid)) == 0)
      /*
       * We dont enforce that userid must be set before logfiles, so make sure
       * that the old userids, if any, are set before (re)opening logfiles.
       */
      sockscf.uid = olduserid;
#endif /* !SOCKS_CLIENT && !HAVE_PRIVILEGES */

#if !SOCKS_CLIENT 
   sockd_priv(SOCKD_PRIV_PRIVILEGED, PRIV_ON);
#endif /* !SOCKS_CLIENT */

   p = socks_addlogfile(add_to_errorlog ? &sockscf.errlog : &sockscf.log, (yyvsp[(1) - (1)].string));

#if !SOCKS_CLIENT 
   sockd_priv(SOCKD_PRIV_PRIVILEGED, PRIV_OFF);
#endif /* !SOCKS_CLIENT */

#if !SOCKS_CLIENT && !HAVE_PRIVILEGES
   if (p != 0 && sockscf.state.inited) {
      /* try again with original euid, before giving up. */
      sockscf.uid.privileged       = sockscf.state.euid;
      sockscf.uid.privileged_isset = 1;

      sockd_priv(SOCKD_PRIV_PRIVILEGED, PRIV_ON);
      p= socks_addlogfile(add_to_errorlog ? &sockscf.errlog : &sockscf.log, (yyvsp[(1) - (1)].string));
      sockd_priv(SOCKD_PRIV_PRIVILEGED, PRIV_OFF);
   }
#endif /* !SOCKS_CLIENT && !HAVE_PRIVILEGES */

   if (p != 0)
      /*
       * bad, but what else can we do?
       */
      yyerror("failed to add logfile %s", (yyvsp[(1) - (1)].string));


#if !SOCKS_CLIENT && !HAVE_PRIVILEGES
   sockscf.uid = currentuserid;
#endif /* !SOCKS_CLIENT && !HAVE_PRIVILEGES */
}
    break;

  case 97:

/* Line 1806 of yacc.c  */
#line 702 "config_parse.y"
    {
#if !SOCKS_CLIENT
      sockscf.child.maxidle.negotiate = SOCKD_FREESLOTS_NEGOTIATE * 2;
      sockscf.child.maxidle.request   = SOCKD_FREESLOTS_REQUEST   * 2;
      sockscf.child.maxidle.io        = SOCKD_FREESLOTS_IO        * 2;
   }
    break;

  case 98:

/* Line 1806 of yacc.c  */
#line 708 "config_parse.y"
    {
      bzero(&sockscf.child.maxidle, sizeof(sockscf.child.maxidle));
   }
    break;

  case 99:

/* Line 1806 of yacc.c  */
#line 711 "config_parse.y"
    {
      CHECKNUMBER((yyvsp[(3) - (3)].string), >=, 0);
      sockscf.child.maxrequests = (size_t)atol((yyvsp[(3) - (3)].string));
#endif /* !SOCKS_CLIENT */
   }
    break;

  case 103:

/* Line 1806 of yacc.c  */
#line 723 "config_parse.y"
    {
#if !SOCKS_CLIENT
#if HAVE_PRIVILEGES
      yyerror("userid-settings not used on platforms with privileges");
#else
      sockscf.uid.privileged         = (yyvsp[(3) - (3)].uid);
      sockscf.uid.privileged_isset   = 1;
#endif /* !HAVE_PRIVILEGES */
#endif /* !SOCKS_CLIENT */
   }
    break;

  case 104:

/* Line 1806 of yacc.c  */
#line 735 "config_parse.y"
    {
#if !SOCKS_CLIENT
#if HAVE_PRIVILEGES
      yyerror("userid-settings not used on platforms with privileges");
#else
      sockscf.uid.unprivileged         = (yyvsp[(3) - (3)].uid);
      sockscf.uid.unprivileged_isset   = 1;
#endif /* !HAVE_PRIVILEGES */
#endif /* !SOCKS_CLIENT */
   }
    break;

  case 105:

/* Line 1806 of yacc.c  */
#line 747 "config_parse.y"
    {
#if HAVE_LIBWRAP && (!SOCKS_CLIENT)
#if HAVE_PRIVILEGES
      yyerror("userid-settings not used on platforms with privileges");
#else
      sockscf.uid.libwrap         = (yyvsp[(3) - (3)].uid);
      sockscf.uid.libwrap_isset   = 1;
#endif /* !HAVE_PRIVILEGES */
#else  /* !HAVE_LIBWRAP && (!SOCKS_CLIENT) */
      yyerror("libwrapsupport not compiled in");
#endif /* !HAVE_LIBWRAP (!SOCKS_CLIENT)*/
   }
    break;

  case 106:

/* Line 1806 of yacc.c  */
#line 762 "config_parse.y"
    {
      struct passwd *pw;

      if ((pw = socks_getpwnam((yyvsp[(1) - (1)].string))) == NULL)
         serrx(EXIT_FAILURE, "no such user \"%s\"", (yyvsp[(1) - (1)].string));
      else
         (yyval.uid) = pw->pw_uid;
   }
    break;

  case 107:

/* Line 1806 of yacc.c  */
#line 772 "config_parse.y"
    {
#if !SOCKS_CLIENT
      CHECKNUMBER((yyvsp[(3) - (3)].string), >=, 0);
      timeout->tcpio = (size_t)atol((yyvsp[(3) - (3)].string));
      timeout->udpio = timeout->tcpio;
   }
    break;

  case 108:

/* Line 1806 of yacc.c  */
#line 778 "config_parse.y"
    {
      CHECKNUMBER((yyvsp[(3) - (3)].string), >=, 0);
      timeout->tcpio = (size_t)atol((yyvsp[(3) - (3)].string));
   }
    break;

  case 109:

/* Line 1806 of yacc.c  */
#line 782 "config_parse.y"
    {
      CHECKNUMBER((yyvsp[(3) - (3)].string), >=, 0);
      timeout->udpio = (size_t)atol((yyvsp[(3) - (3)].string));
#endif /* !SOCKS_CLIENT */
   }
    break;

  case 110:

/* Line 1806 of yacc.c  */
#line 789 "config_parse.y"
    {
#if !SOCKS_CLIENT
      CHECKNUMBER((yyvsp[(3) - (3)].string), >=, 0);
      timeout->negotiate = (size_t)atol((yyvsp[(3) - (3)].string));
#endif /* !SOCKS_CLIENT */
   }
    break;

  case 111:

/* Line 1806 of yacc.c  */
#line 797 "config_parse.y"
    {
      CHECKNUMBER((yyvsp[(3) - (3)].string), >=, 0);
      timeout->connect = (size_t)atol((yyvsp[(3) - (3)].string));
   }
    break;

  case 112:

/* Line 1806 of yacc.c  */
#line 803 "config_parse.y"
    {
#if !SOCKS_CLIENT
      CHECKNUMBER((yyvsp[(3) - (3)].string), >=, 0);
      timeout->tcp_fin_wait = (size_t)atol((yyvsp[(3) - (3)].string));
#endif /* !SOCKS_CLIENT */
   }
    break;

  case 113:

/* Line 1806 of yacc.c  */
#line 812 "config_parse.y"
    {
#if !SOCKS_CLIENT
      if (sockscf.option.debugrunopt == -1)
#endif /* !SOCKS_CLIENT */
          sockscf.option.debug = atoi((yyvsp[(3) - (3)].string));
   }
    break;

  case 116:

/* Line 1806 of yacc.c  */
#line 824 "config_parse.y"
    {
#if !SOCKS_CLIENT
#if HAVE_LIBWRAP
      if ((hosts_allow_table = strdup((yyvsp[(3) - (3)].string))) == NULL)
         yyerror(NOMEM);
      slog(LOG_DEBUG, "libwrap.allow: %s", hosts_allow_table);
#else
      yyerror("libwrap.allow requires libwrap library");
#endif /* HAVE_LIBWRAP */
#endif /* !SOCKS_CLIENT */
   }
    break;

  case 117:

/* Line 1806 of yacc.c  */
#line 837 "config_parse.y"
    {
#if !SOCKS_CLIENT
#if HAVE_LIBWRAP
      if ((hosts_deny_table = strdup((yyvsp[(3) - (3)].string))) == NULL)
         yyerror(NOMEM);
      slog(LOG_DEBUG, "libwrap.deny: %s", hosts_deny_table);
#else
      yyerror("libwrap.deny requires libwrap library");
#endif /* HAVE_LIBWRAP */
#endif /* !SOCKS_CLIENT */
   }
    break;

  case 118:

/* Line 1806 of yacc.c  */
#line 850 "config_parse.y"
    {
#if !SOCKS_CLIENT
#if HAVE_LIBWRAP
      sockscf.option.hosts_access = 1;
#else
      yyerror("libwrap.hosts_access requires libwrap library");
#endif /* HAVE_LIBWRAP */
   }
    break;

  case 119:

/* Line 1806 of yacc.c  */
#line 858 "config_parse.y"
    {
#if HAVE_LIBWRAP
      sockscf.option.hosts_access = 0;
#else
      yyerror("libwrap.hosts_access requires libwrap library");
#endif /* HAVE_LIBWRAP */
#endif /* !SOCKS_CLIENT */
   }
    break;

  case 120:

/* Line 1806 of yacc.c  */
#line 868 "config_parse.y"
    {
#if !SOCKS_CLIENT
      sockscf.udpconnectdst = 1;
   }
    break;

  case 121:

/* Line 1806 of yacc.c  */
#line 872 "config_parse.y"
    {
      sockscf.udpconnectdst = 0;
#endif /* !SOCKS_CLIENT */
   }
    break;

  case 123:

/* Line 1806 of yacc.c  */
#line 882 "config_parse.y"
    {
#if !SOCKS_CLIENT
      sockscf.compat.sameport = 1;
   }
    break;

  case 124:

/* Line 1806 of yacc.c  */
#line 886 "config_parse.y"
    {
      sockscf.compat.draft_5_05 = 1;
#endif /* !SOCKS_CLIENT */
   }
    break;

  case 128:

/* Line 1806 of yacc.c  */
#line 899 "config_parse.y"
    {
         sockscf.resolveprotocol = RESOLVEPROTOCOL_FAKE;
   }
    break;

  case 129:

/* Line 1806 of yacc.c  */
#line 902 "config_parse.y"
    {
#if HAVE_NO_RESOLVESTUFF
         yyerror("resolveprotocol keyword not supported on this installation");
#else
         sockscf.resolveprotocol = RESOLVEPROTOCOL_TCP;
#endif /* !HAVE_NO_RESOLVESTUFF */
   }
    break;

  case 130:

/* Line 1806 of yacc.c  */
#line 909 "config_parse.y"
    {
         sockscf.resolveprotocol = RESOLVEPROTOCOL_UDP;
   }
    break;

  case 131:

/* Line 1806 of yacc.c  */
#line 914 "config_parse.y"
    {
#if !SOCKS_CLIENT
      CHECKNUMBER((yyvsp[(7) - (7)].string), >=, 0);
      sockscf.socket.udp.sndbuf = (size_t)atol((yyvsp[(7) - (7)].string));
   }
    break;

  case 132:

/* Line 1806 of yacc.c  */
#line 919 "config_parse.y"
    {
      CHECKNUMBER((yyvsp[(7) - (7)].string), >=, 0);
      sockscf.socket.udp.rcvbuf = (size_t)atol((yyvsp[(7) - (7)].string));
   }
    break;

  case 133:

/* Line 1806 of yacc.c  */
#line 923 "config_parse.y"
    {
      CHECKNUMBER((yyvsp[(7) - (7)].string), >=, 0);
      sockscf.socket.tcp.sndbuf = (size_t)atol((yyvsp[(7) - (7)].string));
   }
    break;

  case 134:

/* Line 1806 of yacc.c  */
#line 927 "config_parse.y"
    {
      CHECKNUMBER((yyvsp[(7) - (7)].string), >=, 0);
      sockscf.socket.tcp.rcvbuf = (size_t)atol((yyvsp[(7) - (7)].string));
#if BAREFOOTD
   }
    break;

  case 135:

/* Line 1806 of yacc.c  */
#line 932 "config_parse.y"
    {
      CHECKNUMBER((yyvsp[(7) - (7)].string), >=, 0);
      sockscf.socket.clientside_udp.sndbuf = (size_t)atol((yyvsp[(7) - (7)].string));
   }
    break;

  case 136:

/* Line 1806 of yacc.c  */
#line 936 "config_parse.y"
    {
      CHECKNUMBER((yyvsp[(7) - (7)].string), >=, 0);
      sockscf.socket.clientside_udp.rcvbuf = (size_t)atol((yyvsp[(7) - (7)].string));
#endif /* BAREFOOTD */

#endif /* !SOCKS_CLIENT */
   }
    break;

  case 138:

/* Line 1806 of yacc.c  */
#line 949 "config_parse.y"
    {
#if !SOCKS_CLIENT
         sockscf.srchost.nodnsmismatch = 1;
   }
    break;

  case 139:

/* Line 1806 of yacc.c  */
#line 953 "config_parse.y"
    {
         sockscf.srchost.nodnsunknown = 1;
   }
    break;

  case 140:

/* Line 1806 of yacc.c  */
#line 956 "config_parse.y"
    {
         sockscf.srchost.checkreplyauth = 1;
#endif /* !SOCKS_CLIENT */
   }
    break;

  case 143:

/* Line 1806 of yacc.c  */
#line 966 "config_parse.y"
    {
#if COVENANT
   if (strlen((yyvsp[(3) - (3)].string)) >= sizeof(sockscf.realmname))
      yyerror("realmname \"%s\" is too long.  Recompilation of %s required "
              "is required if you want to use a name longer than %d characters",
               (yyvsp[(3) - (3)].string), PACKAGE,
               sizeof(sockscf.realmname) - 1);

   strcpy(sockscf.realmname, (yyvsp[(3) - (3)].string));
#else /* !COVENANT */
   yyerror("unknown keyword \"%s\"", (yyvsp[(1) - (3)].string));
#endif /* !COVENANT */
}
    break;

  case 147:

/* Line 1806 of yacc.c  */
#line 988 "config_parse.y"
    {
#if SOCKS_SERVER
      methodv  = sockscf.methodv;
      methodc  = &sockscf.methodc;
      *methodc = 0; /* reset. */
#else
      yyerror("\"clientmethod\" is used for the global method line in %s, "
              "not \"%s\"",
              PACKAGE, (yyvsp[(1) - (2)].string));
#endif /* !SOCKS_SERVER */
   }
    break;

  case 149:

/* Line 1806 of yacc.c  */
#line 1001 "config_parse.y"
    {
#if !SOCKS_CLIENT
   methodv  = sockscf.clientmethodv;
   methodc  = &sockscf.clientmethodc;
   *methodc = 0; /* reset. */
#endif /* !SOCKS_CLIENT */
   }
    break;

  case 151:

/* Line 1806 of yacc.c  */
#line 1010 "config_parse.y"
    {
      ADDMETHOD(AUTHMETHOD_NONE);
   }
    break;

  case 152:

/* Line 1806 of yacc.c  */
#line 1013 "config_parse.y"
    {
#if !HAVE_GSSAPI
      yyerror("method %s requires gssapi library", AUTHMETHOD_GSSAPIs);
#else
      ADDMETHOD(AUTHMETHOD_GSSAPI);
#endif /* !HAVE_GSSAPI */
   }
    break;

  case 153:

/* Line 1806 of yacc.c  */
#line 1020 "config_parse.y"
    {
      ADDMETHOD(AUTHMETHOD_UNAME);
   }
    break;

  case 154:

/* Line 1806 of yacc.c  */
#line 1023 "config_parse.y"
    {
#if HAVE_LIBWRAP
      ADDMETHOD(AUTHMETHOD_RFC931);
#else
      yyerror("method %s requires libwrap library", AUTHMETHOD_RFC931s);
#endif /* HAVE_LIBWRAP */
   }
    break;

  case 155:

/* Line 1806 of yacc.c  */
#line 1030 "config_parse.y"
    {
#if HAVE_PAM
      ADDMETHOD(AUTHMETHOD_PAM);
#else /* !HAVE_PAM */
      yyerror("method %s requires pam library", AUTHMETHOD_PAMs);
#endif /* HAVE_PAM */
   }
    break;

  case 156:

/* Line 1806 of yacc.c  */
#line 1037 "config_parse.y"
    {
#if HAVE_BSDAUTH
      ADDMETHOD(AUTHMETHOD_BSDAUTH);
#else /* !HAVE_PAM */
      yyerror("method %s requires bsd authentication", AUTHMETHOD_BSDAUTHs);
#endif /* HAVE_PAM */
   }
    break;

  case 157:

/* Line 1806 of yacc.c  */
#line 1050 "config_parse.y"
    {

#if !SOCKS_CLIENT
      rule.src         = src;
      rule.dst         = dst;
      rule.rdr_from    = rdr_from;
      rule.rdr_to      = rdr_to;

#if BAREFOOTD
      if (bounce_to.atype == SOCKS_ADDR_NOTSET) {
         if (rule.verdict == VERDICT_PASS)
            yyerror("no address traffic should bounce to has been given");
         else {
            /*
             * allow no bounce-to if it is a block, as the bounce-to address
             * will not be used in any case then.
             */
            bounce_to.atype                 = SOCKS_ADDR_IPV4;
            bounce_to.addr.ipv4.ip.s_addr   = htonl(INADDR_ANY);
            bounce_to.addr.ipv4.mask.s_addr = htonl(0xffffffff);
            bounce_to.port.tcp              = bounce_to.port.udp = htons(0);
            bounce_to.operator              = none;
         }
      }

      rule.bounce_to = bounce_to;
#endif /* BAREFOOTD */

      addclientrule(&rule);

      rulereset();
#endif /* !SOCKS_CLIENT */
   }
    break;

  case 159:

/* Line 1806 of yacc.c  */
#line 1086 "config_parse.y"
    {
#if !SOCKS_CLIENT
         checkmodule("bandwidth");
#endif /* !SOCKS_CLIENT */
   }
    break;

  case 160:

/* Line 1806 of yacc.c  */
#line 1091 "config_parse.y"
    {
#if !BAREFOOTD
         yyerror("unsupported option");
#endif /* !BAREFOOTD */
   }
    break;

  case 161:

/* Line 1806 of yacc.c  */
#line 1096 "config_parse.y"
    {
#if !SOCKS_CLIENT
         checkmodule("redirect");
#endif /* !SOCKS_CLIENT */
   }
    break;

  case 162:

/* Line 1806 of yacc.c  */
#line 1103 "config_parse.y"
    { (yyval.string) = NULL; }
    break;

  case 164:

/* Line 1806 of yacc.c  */
#line 1107 "config_parse.y"
    {
#if !SOCKS_CLIENT
      rule.src         = src;
      rule.dst         = dst;
      rule.rdr_from    = rdr_from;
      rule.rdr_to      = rdr_to;

#if !SOCKS_SERVER
   yyerror("socks-rules are not used in %s", PACKAGE);
#endif /* !SOCKS_SERVER */

      addsocksrule(&rule);
      rulereset();
#endif /* !SOCKS_CLIENT */
   }
    break;

  case 166:

/* Line 1806 of yacc.c  */
#line 1126 "config_parse.y"
    {
#if !SOCKS_CLIENT
         checkmodule("bandwidth");
#endif /* !SOCKS_CLIENT */
   }
    break;

  case 171:

/* Line 1806 of yacc.c  */
#line 1135 "config_parse.y"
    {
#if !SOCKS_CLIENT
         checkmodule("redirect");
#endif /* !SOCKS_CLIENT */
   }
    break;

  case 172:

/* Line 1806 of yacc.c  */
#line 1142 "config_parse.y"
    { (yyval.string) = NULL; }
    break;

  case 214:

/* Line 1806 of yacc.c  */
#line 1186 "config_parse.y"
    {
#if !BAREFOOTD
         yyerror("unsupported option");
#endif /* !BAREFOOTD */
   }
    break;

  case 215:

/* Line 1806 of yacc.c  */
#line 1191 "config_parse.y"
    {
#if !SOCKS_CLIENT
         checkmodule("session");
#endif /* !SOCKS_CLIENT */
   }
    break;

  case 216:

/* Line 1806 of yacc.c  */
#line 1198 "config_parse.y"
    {
#if SOCKS_SERVER
#if HAVE_LDAP && HAVE_OPENLDAP
      ldap->debug = atoi((yyvsp[(3) - (3)].string));
   }
    break;

  case 217:

/* Line 1806 of yacc.c  */
#line 1203 "config_parse.y"
    {
      ldap->debug = -atoi((yyvsp[(4) - (4)].string));
 #else /* !HAVE_LDAP */
      yyerror("ldap debug support requires openldap support");
#endif /* !HAVE_LDAP */
#endif /* SOCKS_SERVER */
   }
    break;

  case 218:

/* Line 1806 of yacc.c  */
#line 1212 "config_parse.y"
    {
#if SOCKS_SERVER
#if HAVE_LDAP
      if (strlen((yyvsp[(3) - (3)].string)) >= sizeof(state.ldap.domain))
         yyerror("filter too long");
      strcpy(ldap->domain, (yyvsp[(3) - (3)].string));
#else /* !HAVE_LDAP */
      yyerror("no LDAP support configured for %s/server", PACKAGE);
#endif /* !HAVE_LDAP */
#endif /* SOCKS_SERVER */
   }
    break;

  case 219:

/* Line 1806 of yacc.c  */
#line 1225 "config_parse.y"
    {
#if SOCKS_SERVER
#if HAVE_LDAP && HAVE_OPENLDAP
      ldap->mdepth = atoi((yyvsp[(3) - (3)].string));
#else /* !HAVE_LDAP */
      yyerror("ldap debug support requires openldap support");
#endif /* !HAVE_LDAP */
#endif /* SOCKS_SERVER */
   }
    break;

  case 220:

/* Line 1806 of yacc.c  */
#line 1236 "config_parse.y"
    {
#if SOCKS_SERVER
#if HAVE_LDAP
      if (strlen((yyvsp[(3) - (3)].string)) >= sizeof(state.ldap.certfile))
         yyerror("ca cert file name too long");
      strcpy(ldap->certfile, (yyvsp[(3) - (3)].string));
#else /* !HAVE_LDAP */
      yyerror("no LDAP support configured for %s/server", PACKAGE);
#endif /* !HAVE_LDAP */
#endif /* SOCKS_SERVER */
   }
    break;

  case 221:

/* Line 1806 of yacc.c  */
#line 1249 "config_parse.y"
    {
#if SOCKS_SERVER
#if HAVE_LDAP
      if (strlen((yyvsp[(3) - (3)].string)) >= sizeof(state.ldap.certpath))
         yyerror("cert db path too long");
      strcpy(ldap->certpath, (yyvsp[(3) - (3)].string));
#else /* !HAVE_LDAP */
      yyerror("no LDAP support configured for %s/server", PACKAGE);
#endif /* !HAVE_LDAP */
#endif /* SOCKS_SERVER */
   }
    break;

  case 222:

/* Line 1806 of yacc.c  */
#line 1262 "config_parse.y"
    {
#if SOCKS_SERVER
#if HAVE_LDAP
      if (addlinkedname(&rule.state.ldap.ldapurl, (yyvsp[(3) - (3)].string)) == NULL)
         yyerror(NOMEM);
#else /* !HAVE_LDAP */
      yyerror("no LDAP support configured for %s/server", PACKAGE);
#endif /* !HAVE_LDAP */
#endif /* SOCKS_SERVER */
   }
    break;

  case 223:

/* Line 1806 of yacc.c  */
#line 1274 "config_parse.y"
    {
#if SOCKS_SERVER
#if HAVE_LDAP
      if (addlinkedname(&rule.state.ldap.ldapbasedn, (yyvsp[(3) - (3)].string)) == NULL)
         yyerror(NOMEM);
#else /* !HAVE_LDAP */
      yyerror("no LDAP support configured for %s/server", PACKAGE);
#endif /* !HAVE_LDAP */
#endif /* SOCKS_SERVER */
   }
    break;

  case 224:

/* Line 1806 of yacc.c  */
#line 1286 "config_parse.y"
    {
#if SOCKS_SERVER
#if HAVE_LDAP
      if (addlinkedname(&rule.state.ldap.ldapbasedn, hextoutf8((yyvsp[(3) - (3)].string), 0)) == NULL)
         yyerror(NOMEM);
#else /* !HAVE_LDAP */
      yyerror("no LDAP support configured for %s/server", PACKAGE);
#endif /* !HAVE_LDAP */
#endif /* SOCKS_SERVER */
   }
    break;

  case 225:

/* Line 1806 of yacc.c  */
#line 1298 "config_parse.y"
    {
#if SOCKS_SERVER
#if HAVE_LDAP
      if (addlinkedname(&rule.state.ldap.ldapbasedn, hextoutf8((yyvsp[(3) - (3)].string), 1)) == NULL)
         yyerror(NOMEM);
#else /* !HAVE_LDAP */
      yyerror("no LDAP support configured for %s/server", PACKAGE);
#endif /* !HAVE_LDAP */
#endif /* SOCKS_SERVER */
   }
    break;

  case 226:

/* Line 1806 of yacc.c  */
#line 1310 "config_parse.y"
    {
#if SOCKS_SERVER
#if HAVE_LDAP
   ldap->port = atoi((yyvsp[(3) - (3)].string));
#else /* !HAVE_LDAP */
   yyerror("no LDAP support configured for %s/server", PACKAGE);
#endif /* !HAVE_LDAP */
#endif /* SOCKS_SERVER */
   }
    break;

  case 227:

/* Line 1806 of yacc.c  */
#line 1321 "config_parse.y"
    {
#if SOCKS_SERVER
#if HAVE_LDAP
   ldap->portssl = atoi((yyvsp[(3) - (3)].string));
#else /* !HAVE_LDAP */
   yyerror("no LDAP support configured for %s/server", PACKAGE);
#endif /* !HAVE_LDAP */
#endif /* SOCKS_SERVER */
   }
    break;

  case 228:

/* Line 1806 of yacc.c  */
#line 1332 "config_parse.y"
    {
#if SOCKS_SERVER
#if HAVE_LDAP
      ldap->ssl = 1;
   }
    break;

  case 229:

/* Line 1806 of yacc.c  */
#line 1337 "config_parse.y"
    {
      ldap->ssl = 0;
#else /* !HAVE_LDAP */
      yyerror("no LDAP support configured for %s/server", PACKAGE);
#endif /* !HAVE_LDAP */
#endif /* SOCKS_SERVER */
   }
    break;

  case 230:

/* Line 1806 of yacc.c  */
#line 1346 "config_parse.y"
    {
#if SOCKS_SERVER
#if HAVE_LDAP
      ldap->auto_off = 1;
   }
    break;

  case 231:

/* Line 1806 of yacc.c  */
#line 1351 "config_parse.y"
    {
      ldap->auto_off = 0;
#else /* !HAVE_LDAP */
      yyerror("no LDAP support configured for %s/server", PACKAGE);
#endif /* !HAVE_LDAP */
#endif /* SOCKS_SERVER */
   }
    break;

  case 232:

/* Line 1806 of yacc.c  */
#line 1360 "config_parse.y"
    {
#if SOCKS_SERVER
#if HAVE_LDAP
      ldap->certcheck = 1;
   }
    break;

  case 233:

/* Line 1806 of yacc.c  */
#line 1365 "config_parse.y"
    {
      ldap->certcheck = 0;
#else /* !HAVE_LDAP */
      yyerror("no LDAP support configured for %s/server", PACKAGE);
#endif /* !HAVE_LDAP */
#endif /* SOCKS_SERVER */
   }
    break;

  case 234:

/* Line 1806 of yacc.c  */
#line 1374 "config_parse.y"
    {
#if SOCKS_SERVER
#if HAVE_LDAP
      ldap->keeprealm = 1;
   }
    break;

  case 235:

/* Line 1806 of yacc.c  */
#line 1379 "config_parse.y"
    {
      ldap->keeprealm = 0;
#else /* !HAVE_LDAP */
      yyerror("no LDAP support configured for %s/server", PACKAGE);
#endif /* !HAVE_LDAP */
#endif /* SOCKS_SERVER */
   }
    break;

  case 236:

/* Line 1806 of yacc.c  */
#line 1388 "config_parse.y"
    {
#if SOCKS_SERVER
#if HAVE_LDAP
      if (strlen((yyvsp[(3) - (3)].string)) >= sizeof(state.ldap.filter))
         yyerror("filter too long");
      strcpy(ldap->filter, (yyvsp[(3) - (3)].string));
#else /* !HAVE_LDAP */
      yyerror("no LDAP support configured for %s/server", PACKAGE);
#endif /* !HAVE_LDAP */
#endif /* SOCKS_SERVER */
   }
    break;

  case 237:

/* Line 1806 of yacc.c  */
#line 1401 "config_parse.y"
    {
#if SOCKS_SERVER
#if HAVE_LDAP
      if (strlen((yyvsp[(3) - (3)].string)) >= sizeof(state.ldap.filter_AD))
         yyerror("AD filter too long");
      strcpy(ldap->filter_AD, (yyvsp[(3) - (3)].string));
#else /* !HAVE_LDAP */
      yyerror("no LDAP support configured for %s/server", PACKAGE);
#endif /* !HAVE_LDAP */
#endif /* SOCKS_SERVER */
   }
    break;

  case 238:

/* Line 1806 of yacc.c  */
#line 1414 "config_parse.y"
    {
#if SOCKS_SERVER
#if HAVE_LDAP
      if (strlen((yyvsp[(3) - (3)].string))/2 >= sizeof(state.ldap.filter))
         yyerror("filter too long");
      strcpy(ldap->filter, hextoutf8((yyvsp[(3) - (3)].string), 2));
#else /* !HAVE_LDAP */
      yyerror("no LDAP support configured for %s/server", PACKAGE);
#endif /* !HAVE_LDAP */
#endif /* SOCKS_SERVER */
   }
    break;

  case 239:

/* Line 1806 of yacc.c  */
#line 1427 "config_parse.y"
    {
#if SOCKS_SERVER
#if HAVE_LDAP
      if (strlen((yyvsp[(3) - (3)].string))/2 >= sizeof(state.ldap.filter_AD))
         yyerror("AD filter too long");
      strcpy(ldap->filter_AD, hextoutf8((yyvsp[(3) - (3)].string),2));
#else /* !HAVE_LDAP */
      yyerror("no LDAP support configured for %s/server", PACKAGE);
#endif /* !HAVE_LDAP */
#endif /* SOCKS_SERVER */
   }
    break;

  case 240:

/* Line 1806 of yacc.c  */
#line 1440 "config_parse.y"
    {
#if SOCKS_SERVER
#if HAVE_LDAP
      if (strlen((yyvsp[(3) - (3)].string)) >= sizeof(state.ldap.attribute))
         yyerror("attribute too long");
      strcpy(ldap->attribute, (yyvsp[(3) - (3)].string));
#else /* !HAVE_LDAP */
      yyerror("no LDAP support configured for %s/server", PACKAGE);
#endif /* !HAVE_LDAP */
#endif /* SOCKS_SERVER */
   }
    break;

  case 241:

/* Line 1806 of yacc.c  */
#line 1453 "config_parse.y"
    {
#if SOCKS_SERVER
#if HAVE_LDAP
      if (strlen((yyvsp[(3) - (3)].string)) >= sizeof(state.ldap.attribute_AD))
         yyerror("AD attribute too long");
      strcpy(ldap->attribute_AD, (yyvsp[(3) - (3)].string));
#else /* !HAVE_LDAP */
      yyerror("no LDAP support configured for %s/server", PACKAGE);
#endif /* !HAVE_LDAP */
#endif /* SOCKS_SERVER */
   }
    break;

  case 242:

/* Line 1806 of yacc.c  */
#line 1466 "config_parse.y"
    {
#if SOCKS_SERVER
#if HAVE_LDAP
      if (strlen((yyvsp[(3) - (3)].string)) / 2 >= sizeof(state.ldap.attribute))
         yyerror("attribute too long");
      strcpy(ldap->attribute, hextoutf8((yyvsp[(3) - (3)].string), 2));
#else /* !HAVE_LDAP */
      yyerror("no LDAP support configured for %s/server", PACKAGE);
#endif /* !HAVE_LDAP */
#endif /* SOCKS_SERVER */
   }
    break;

  case 243:

/* Line 1806 of yacc.c  */
#line 1479 "config_parse.y"
    {
#if SOCKS_SERVER
#if HAVE_LDAP
      if (strlen((yyvsp[(3) - (3)].string)) / 2 >= sizeof(state.ldap.attribute_AD))
         yyerror("AD attribute too long");
      strcpy(ldap->attribute_AD, hextoutf8((yyvsp[(3) - (3)].string), 2));
#else /* !HAVE_LDAP */
      yyerror("no LDAP support configured for %s/server", PACKAGE);
#endif /* !HAVE_LDAP */
#endif /* SOCKS_SERVER */
   }
    break;

  case 244:

/* Line 1806 of yacc.c  */
#line 1492 "config_parse.y"
    {
#if SOCKS_SERVER
#if HAVE_LDAP
      if (addlinkedname(&rule.ldapgroup, hextoutf8((yyvsp[(3) - (3)].string), 0)) == NULL)
         yyerror(NOMEM);
#else /* !HAVE_LDAP */
      yyerror("no LDAP support configured for %s/server", PACKAGE);
#endif /* !HAVE_LDAP */
#endif /* SOCKS_SERVER */
   }
    break;

  case 245:

/* Line 1806 of yacc.c  */
#line 1504 "config_parse.y"
    {
#if SOCKS_SERVER
#if HAVE_LDAP
      if (addlinkedname(&rule.ldapgroup, hextoutf8((yyvsp[(3) - (3)].string), 1)) == NULL)
         yyerror(NOMEM);
#else /* !HAVE_LDAP */
      yyerror("no LDAP support configured for %s/server", PACKAGE);
#endif /* !HAVE_LDAP */
#endif /* SOCKS_SERVER */
   }
    break;

  case 246:

/* Line 1806 of yacc.c  */
#line 1516 "config_parse.y"
    {
#if SOCKS_SERVER
#if HAVE_LDAP
      if (addlinkedname(&rule.ldapgroup, asciitoutf8((yyvsp[(3) - (3)].string))) == NULL)
         yyerror(NOMEM);
#else /* !HAVE_LDAP */
      yyerror("no LDAP support configured for %s/server", PACKAGE);
#endif /* !HAVE_LDAP */
#endif /* SOCKS_SERVER */
   }
    break;

  case 247:

/* Line 1806 of yacc.c  */
#line 1528 "config_parse.y"
    {
#if SOCKS_SERVER
#if HAVE_LDAP
      if (addlinkedname(&rule.ldapserver, (yyvsp[(3) - (3)].string)) == NULL)
         yyerror(NOMEM);
#else /* !HAVE_LDAP */
      yyerror("no LDAP support configured for %s/server", PACKAGE);
#endif /* !HAVE_LDAP */
#endif /* SOCKS_SERVER */
   }
    break;

  case 248:

/* Line 1806 of yacc.c  */
#line 1540 "config_parse.y"
    {
#if HAVE_LDAP
#if SOCKS_SERVER
      if (strlen((yyvsp[(3) - (3)].string)) >= sizeof(state.ldap.keytab))
         yyerror("keytab name too long");
      strcpy(ldap->keytab, (yyvsp[(3) - (3)].string));
#else
      yyerror("ldap keytab only applicable to Dante server");
#endif /* SOCKS_SERVER */
#else
      yyerror("no LDAP support configured for %s/server", PACKAGE);
#endif /* HAVE_LDAP */
   }
    break;

  case 250:

/* Line 1806 of yacc.c  */
#line 1558 "config_parse.y"
    {
#if HAVE_GSSAPI
      gssapiencryption->nec = 1;
#else
      yyerror("method %s requires gssapi library", AUTHMETHOD_GSSAPIs);
#endif /* HAVE_GSSAPI */
   }
    break;

  case 253:

/* Line 1806 of yacc.c  */
#line 1572 "config_parse.y"
    {
#if !SOCKS_CLIENT
      ruleinit(&rule);
      rule.verdict   = VERDICT_BLOCK;
   }
    break;

  case 254:

/* Line 1806 of yacc.c  */
#line 1577 "config_parse.y"
    {
      ruleinit(&rule);
      rule.verdict   = VERDICT_PASS;
#endif /* !SOCKS_CLIENT */
   }
    break;

  case 258:

/* Line 1806 of yacc.c  */
#line 1591 "config_parse.y"
    {
         command->bind = 1;
   }
    break;

  case 259:

/* Line 1806 of yacc.c  */
#line 1594 "config_parse.y"
    {
         command->connect = 1;
   }
    break;

  case 260:

/* Line 1806 of yacc.c  */
#line 1597 "config_parse.y"
    {
         command->udpassociate = 1;
   }
    break;

  case 261:

/* Line 1806 of yacc.c  */
#line 1603 "config_parse.y"
    {
         command->bindreply = 1;
   }
    break;

  case 262:

/* Line 1806 of yacc.c  */
#line 1607 "config_parse.y"
    {
         command->udpreply = 1;
   }
    break;

  case 266:

/* Line 1806 of yacc.c  */
#line 1620 "config_parse.y"
    {
      protocol->tcp = 1;
   }
    break;

  case 267:

/* Line 1806 of yacc.c  */
#line 1623 "config_parse.y"
    {
      protocol->udp = 1;
   }
    break;

  case 273:

/* Line 1806 of yacc.c  */
#line 1640 "config_parse.y"
    {
#if !SOCKS_CLIENT
   static shmem_object_t ssinit;

   CHECKNUMBER((yyvsp[(3) - (3)].string), >=, 0);

   if (pidismother(sockscf.state.pid) == 1) {
      if ((rule.ss = malloc(sizeof(*rule.ss))) == NULL)
         yyerror("failed to malloc %lu bytes for ss memory",
         (unsigned long)sizeof(*rule.ss));

      *rule.ss                       = ssinit;
      rule.ss->object.ss.maxsessions = (size_t)atol((yyvsp[(3) - (3)].string));
   }
   else
      rule.ss = &ssinit;

   rule.ss_fd = -1;
#endif /* !SOCKS_CLIENT */
}
    break;

  case 274:

/* Line 1806 of yacc.c  */
#line 1662 "config_parse.y"
    {
#if !SOCKS_CLIENT
   static shmem_object_t bwmeminit;

   CHECKNUMBER((yyvsp[(3) - (3)].string), >=, 0);

   if (pidismother(sockscf.state.pid) == 1) {
      if ((rule.bw = malloc(sizeof(*rule.bw))) == NULL)
         yyerror("failed to malloc %lu bytes for bw memory",
         (unsigned long)sizeof(*rule.bw));

      *rule.bw                  = bwmeminit;
      rule.bw->object.bw.maxbps = (size_t)atol((yyvsp[(3) - (3)].string));
   }
   else
      rule.bw = &bwmeminit;

   rule.bw_fd = -1;
#endif /* !SOCKS_CLIENT */
}
    break;

  case 276:

/* Line 1806 of yacc.c  */
#line 1688 "config_parse.y"
    {
#if !SOCKS_CLIENT
   rule.log.connect = 1;
   }
    break;

  case 277:

/* Line 1806 of yacc.c  */
#line 1692 "config_parse.y"
    {
         rule.log.data = 1;
   }
    break;

  case 278:

/* Line 1806 of yacc.c  */
#line 1695 "config_parse.y"
    {
         rule.log.disconnect = 1;
   }
    break;

  case 279:

/* Line 1806 of yacc.c  */
#line 1698 "config_parse.y"
    {
         rule.log.error = 1;
   }
    break;

  case 280:

/* Line 1806 of yacc.c  */
#line 1701 "config_parse.y"
    {
         rule.log.iooperation = 1;
#endif /* !SOCKS_CLIENT */
   }
    break;

  case 283:

/* Line 1806 of yacc.c  */
#line 1712 "config_parse.y"
    {
#if HAVE_PAM && (!SOCKS_CLIENT)
      if (strlen((yyvsp[(3) - (3)].string)) >= sizeof(rule.state.pamservicename))
         yyerror("servicename too long");
      strcpy(rule.state.pamservicename, (yyvsp[(3) - (3)].string));
#else
      yyerror("pam support not compiled in");
#endif /* HAVE_PAM && (!SOCKS_CLIENT) */
   }
    break;

  case 284:

/* Line 1806 of yacc.c  */
#line 1723 "config_parse.y"
    {
#if HAVE_BSDAUTH && SOCKS_SERVER
      if (strlen((yyvsp[(3) - (3)].string)) >= sizeof(rule.state.bsdauthstylename))
         yyerror("bsdauthstyle too long");
      strcpy(rule.state.bsdauthstylename, (yyvsp[(3) - (3)].string));
#else
      yyerror("bsdauth support not compiled in");
#endif /* HAVE_BSDAUTH && SOCKS_SERVER */
   }
    break;

  case 285:

/* Line 1806 of yacc.c  */
#line 1735 "config_parse.y"
    {
#if HAVE_GSSAPI
      if (strlen((yyvsp[(3) - (3)].string)) >= sizeof(state.gssapiservicename))
         yyerror("service name too long");
      strcpy(gssapiservicename, (yyvsp[(3) - (3)].string));
#else
      yyerror("gssapi support not compiled in");
#endif /* HAVE_GSSAPI */
   }
    break;

  case 286:

/* Line 1806 of yacc.c  */
#line 1746 "config_parse.y"
    {
#if HAVE_GSSAPI
#if SOCKS_SERVER
      if (strlen((yyvsp[(3) - (3)].string)) >= sizeof(state.gssapikeytab))
         yyerror("keytab name too long");
      strcpy(gssapikeytab, (yyvsp[(3) - (3)].string));
#else
      yyerror("gssapi keytab only applicable to Dante server");
#endif /* SOCKS_SERVER */
#else
      yyerror("gssapi support not compiled in");
#endif /* HAVE_GSSAPI */
   }
    break;

  case 288:

/* Line 1806 of yacc.c  */
#line 1764 "config_parse.y"
    {
#if HAVE_GSSAPI
      gssapiencryption->clear           = 1;
      gssapiencryption->integrity       = 1;
      gssapiencryption->confidentiality = 1;
   }
    break;

  case 289:

/* Line 1806 of yacc.c  */
#line 1770 "config_parse.y"
    {
      gssapiencryption->clear = 1;
   }
    break;

  case 290:

/* Line 1806 of yacc.c  */
#line 1773 "config_parse.y"
    {
      gssapiencryption->integrity = 1;
   }
    break;

  case 291:

/* Line 1806 of yacc.c  */
#line 1776 "config_parse.y"
    {
      gssapiencryption->confidentiality = 1;
   }
    break;

  case 292:

/* Line 1806 of yacc.c  */
#line 1779 "config_parse.y"
    {
      yyerror("gssapi per-message encryption not supported");
#else
      yyerror("gssapi support not compiled in");
#endif /* HAVE_GSSAPI */
   }
    break;

  case 296:

/* Line 1806 of yacc.c  */
#line 1794 "config_parse.y"
    {
#if HAVE_LIBWRAP && (!SOCKS_CLIENT)
      struct request_info request;
      char libwrap[LIBWRAPBUF];

      if (strlen((yyvsp[(3) - (3)].string)) >= sizeof(rule.libwrap))
         yyerror("libwrapline too long, make LIBWRAPBUF bigger");
      strcpy(rule.libwrap, (yyvsp[(3) - (3)].string));

      /* libwrap modifies the passed buffer. */
      SASSERTX(strlen(rule.libwrap) < sizeof(libwrap));
      strcpy(libwrap, rule.libwrap);

      ++dry_run;
      request_init(&request, RQ_FILE, -1, RQ_DAEMON, __progname, 0);
      if (setjmp(tcpd_buf) != 0)
         yyerror("bad libwrap line");
      process_options(libwrap, &request);
      --dry_run;

#else
      yyerror("libwrap support not compiled in");
#endif /* HAVE_LIBWRAP && (!SOCKS_CLIENT) */
   }
    break;

  case 311:

/* Line 1806 of yacc.c  */
#line 1848 "config_parse.y"
    { (yyval.string) = NULL; }
    break;

  case 313:

/* Line 1806 of yacc.c  */
#line 1852 "config_parse.y"
    {
      addrinit(&src, 1);
   }
    break;

  case 314:

/* Line 1806 of yacc.c  */
#line 1857 "config_parse.y"
    {
      addrinit(&dst,
#if SOCKS_SERVER
               1
#else /* BAREFOOT || COVENANT */
               0 /* the address the server should bind, so must be /32. */
#endif /*  BAREFOOT || COVENANT */
      );
   }
    break;

  case 315:

/* Line 1806 of yacc.c  */
#line 1868 "config_parse.y"
    {
      addrinit(&rdr_from, 1);
   }
    break;

  case 316:

/* Line 1806 of yacc.c  */
#line 1873 "config_parse.y"
    {
      addrinit(&rdr_to, 1);
   }
    break;

  case 317:

/* Line 1806 of yacc.c  */
#line 1878 "config_parse.y"
    {
#if BAREFOOTD
      addrinit(&bounce_to, 0);
#endif /* BAREFOOTD */
   }
    break;

  case 318:

/* Line 1806 of yacc.c  */
#line 1886 "config_parse.y"
    {
      gwaddrinit(&gw);
   }
    break;

  case 323:

/* Line 1806 of yacc.c  */
#line 1898 "config_parse.y"
    {
         if (netmask_required)
            yyerror("no netmask given");
         else
            netmask->s_addr = htonl(0xffffffff);
       }
    break;

  case 332:

/* Line 1806 of yacc.c  */
#line 1916 "config_parse.y"
    {
      *atype = SOCKS_ADDR_IPV4;

      if (inet_aton((yyvsp[(1) - (1)].string), ipaddr) != 1)
         yyerror("bad address: %s", (yyvsp[(1) - (1)].string));
   }
    break;

  case 333:

/* Line 1806 of yacc.c  */
#line 1925 "config_parse.y"
    {
      if (atoi((yyvsp[(1) - (1)].string)) < 0 || atoi((yyvsp[(1) - (1)].string)) > 32)
         yyerror("bad netmask: %s", (yyvsp[(1) - (1)].string));

      netmask->s_addr
      = atoi((yyvsp[(1) - (1)].string)) == 0 ? 0 : htonl(0xffffffff << (32 - atoi((yyvsp[(1) - (1)].string))));
   }
    break;

  case 334:

/* Line 1806 of yacc.c  */
#line 1932 "config_parse.y"
    {
         if (!inet_aton((yyvsp[(1) - (1)].string), netmask))
            yyerror("bad netmask: %s", (yyvsp[(1) - (1)].string));
   }
    break;

  case 335:

/* Line 1806 of yacc.c  */
#line 1938 "config_parse.y"
    {
      *atype = SOCKS_ADDR_DOMAIN;

      if (strlen((yyvsp[(1) - (1)].string)) >= MAXHOSTNAMELEN)
         yyerror("domainname too long");

      strcpy(domain, (yyvsp[(1) - (1)].string));
   }
    break;

  case 336:

/* Line 1806 of yacc.c  */
#line 1948 "config_parse.y"
    {
      *atype = SOCKS_ADDR_IFNAME;

      if (strlen((yyvsp[(1) - (1)].string)) >= MAXIFNAMELEN)
         yyerror("interface name too long");

      strcpy(ifname, (yyvsp[(1) - (1)].string));
   }
    break;

  case 337:

/* Line 1806 of yacc.c  */
#line 1959 "config_parse.y"
    {
      *atype = SOCKS_ADDR_DOMAIN;

      if (strlen((yyvsp[(1) - (1)].string)) >= MAXHOSTNAMELEN)
         yyerror("domain name \"%s\" too long", (yyvsp[(1) - (1)].string));
      strcpy(domain, (yyvsp[(1) - (1)].string));

      proxyprotocol->direct = 1;
   }
    break;

  case 338:

/* Line 1806 of yacc.c  */
#line 1970 "config_parse.y"
    {
      *atype = SOCKS_ADDR_URL;

      if (strlen((yyvsp[(1) - (1)].string)) >= MAXURLLEN)
         yyerror("url \"%s\" too long", (yyvsp[(1) - (1)].string));

      strcpy(url, (yyvsp[(1) - (1)].string));
   }
    break;

  case 339:

/* Line 1806 of yacc.c  */
#line 1981 "config_parse.y"
    { (yyval.string) = NULL; }
    break;

  case 343:

/* Line 1806 of yacc.c  */
#line 1987 "config_parse.y"
    { (yyval.string) = NULL; }
    break;

  case 347:

/* Line 1806 of yacc.c  */
#line 1995 "config_parse.y"
    {
   if (ntohs(*port_tcp) > ntohs(ruleaddr->portend))
      yyerror("end port (%u) can not be less than start port (%u)",
      ntohs(*port_tcp), ntohs(ruleaddr->portend));
   }
    break;

  case 348:

/* Line 1806 of yacc.c  */
#line 2003 "config_parse.y"
    {
      CHECKPORTNUMBER((yyvsp[(1) - (1)].string));
      *port_tcp   = htons((in_port_t)atoi((yyvsp[(1) - (1)].string)));
      *port_udp   = htons((in_port_t)atoi((yyvsp[(1) - (1)].string)));
   }
    break;

  case 349:

/* Line 1806 of yacc.c  */
#line 2010 "config_parse.y"
    {
      CHECKPORTNUMBER((yyvsp[(1) - (1)].string));
      ruleaddr->portend    = htons((in_port_t)atoi((yyvsp[(1) - (1)].string)));
      ruleaddr->operator   = range;
   }
    break;

  case 350:

/* Line 1806 of yacc.c  */
#line 2017 "config_parse.y"
    {
      struct servent   *service;

      if ((service = getservbyname((yyvsp[(1) - (1)].string), "tcp")) == NULL) {
         if (protocol->tcp)
            yyerror("unknown tcp protocol: %s", (yyvsp[(1) - (1)].string));
         *port_tcp = htons(0);
      }
      else
         *port_tcp = (in_port_t)service->s_port;

      if ((service = getservbyname((yyvsp[(1) - (1)].string), "udp")) == NULL) {
         if (protocol->udp)
               yyerror("unknown udp protocol: %s", (yyvsp[(1) - (1)].string));
            *port_udp = htons(0);
      }
      else
         *port_udp = (in_port_t)service->s_port;

      if (*port_tcp == htons(0) && *port_udp == htons(0))
         yyerror("unknown tcp/udp protocol");

      /* if one protocol is unset, set to same as the other. */
      if (*port_tcp == htons(0))
         *port_tcp = *port_udp;
      else if (*port_udp == htons(0))
         *port_udp = *port_tcp;
   }
    break;

  case 351:

/* Line 1806 of yacc.c  */
#line 2048 "config_parse.y"
    {
      *operator = string2operator((yyvsp[(1) - (1)].string));
   }
    break;

  case 353:

/* Line 1806 of yacc.c  */
#line 2056 "config_parse.y"
    {
#if SOCKS_SERVER
   CHECKPORTNUMBER((yyvsp[(1) - (1)].string));
   rule.udprange.start = htons((in_port_t)atoi((yyvsp[(1) - (1)].string)));
#endif /* SOCKS_SERVER */
   }
    break;

  case 354:

/* Line 1806 of yacc.c  */
#line 2064 "config_parse.y"
    {
#if SOCKS_SERVER
   CHECKPORTNUMBER((yyvsp[(1) - (1)].string));
   rule.udprange.end = htons((in_port_t)atoi((yyvsp[(1) - (1)].string)));
   rule.udprange.op  = range;

   if (ntohs(rule.udprange.start) > ntohs(rule.udprange.end))
      yyerror("udp end port (%s) can not be less than udp start port (%u)",
      (yyvsp[(1) - (1)].string), ntohs(rule.udprange.start));
#endif /* SOCKS_SERVER */
   }
    break;

  case 355:

/* Line 1806 of yacc.c  */
#line 2077 "config_parse.y"
    {
#if SOCKS_SERVER
   strncpy(sockscf.authserver, (yyvsp[(3) - (3)].string), sizeof(sockscf.authserver) - 1);
   sockscf.authserver[sizeof(sockscf.authserver) - 1] = 0;
#endif
   }
    break;

  case 356:

/* Line 1806 of yacc.c  */
#line 2085 "config_parse.y"
    {
#if SOCKS_SERVER
   strncpy(sockscf.authusername, (yyvsp[(3) - (3)].string), sizeof(sockscf.authusername) - 1);
   sockscf.authusername[sizeof(sockscf.authusername) - 1] = 0;
#endif
   }
    break;

  case 357:

/* Line 1806 of yacc.c  */
#line 2093 "config_parse.y"
    {
#if SOCKS_SERVER
   strncpy(sockscf.authpassword, (yyvsp[(3) - (3)].string), sizeof(sockscf.authpassword) - 1);
   sockscf.authpassword[sizeof(sockscf.authpassword) - 1] = 0;
#endif
   }
    break;

  case 358:

/* Line 1806 of yacc.c  */
#line 2099 "config_parse.y"
    {
#if SOCKS_SERVER
   sockscf.authpassword[0] = 0;
#endif
   }
    break;

  case 359:

/* Line 1806 of yacc.c  */
#line 2106 "config_parse.y"
    {
#if SOCKS_SERVER
   strncpy(sockscf.authdatabase, (yyvsp[(3) - (3)].string), sizeof(sockscf.authdatabase) - 1);
   sockscf.authdatabase[sizeof(sockscf.authdatabase) - 1] = 0;
#endif
   }
    break;

  case 360:

/* Line 1806 of yacc.c  */
#line 2114 "config_parse.y"
    {
#if SOCKS_SERVER
   strncpy(sockscf.authtable, (yyvsp[(3) - (3)].string), sizeof(sockscf.authtable) - 1);
   sockscf.authtable[sizeof(sockscf.authtable) - 1] = 0;
#endif
   }
    break;

  case 361:

/* Line 1806 of yacc.c  */
#line 2122 "config_parse.y"
    {
#if SOCKS_SERVER
   sockscf.authdelay = atoi((yyvsp[(3) - (3)].string));
#endif
   }
    break;



/* Line 1806 of yacc.c  */
#line 4783 "config_parse.c"
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  *++yyvsp = yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined(yyoverflow) || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}



/* Line 2067 of yacc.c  */
#line 2130 "config_parse.y"


#define INTERACTIVE      0

extern FILE *yyin;

int socks_parseinit;

int
parseconfig(filename)
   const char *filename;
{
   const char *function = "parseconfig()";
   struct stat statbuf;
   int haveconfig;

#if SOCKS_CLIENT /* assume server admin can set things up correctly himself. */
   parseclientenv(&haveconfig);
   if (haveconfig)
      return 0;
#endif


#if !SOCKS_CLIENT
   if (sockscf.state.inited) {
      /* in case needed to reopen config-file. */
      sockd_priv(SOCKD_PRIV_PRIVILEGED, PRIV_ON);

      if (yyin != NULL)
         fclose(yyin);
   }
#endif /* SERVER */

   yyin = fopen(filename, "r");

#if !SOCKS_CLIENT
   if (sockscf.state.inited)
      sockd_priv(SOCKD_PRIV_PRIVILEGED, PRIV_OFF);
#endif /* SERVER */

#if !SOCKS_CLIENT && !HAVE_PRIVILEGES
   if (yyin == NULL && sockscf.state.inited) {
      const struct userid_t currentuserid = sockscf.uid;;

      sockscf.uid.privileged       = sockscf.state.euid;
      sockscf.uid.privileged_isset = 1;

      sockd_priv(SOCKD_PRIV_PRIVILEGED, PRIV_ON);
      yyin = fopen(filename, "r");
      sockd_priv(SOCKD_PRIV_PRIVILEGED, PRIV_OFF);

      sockscf.uid = currentuserid;
   }
#endif /* !SOCKS_CLIENT && !HAVE_PRIVILEGES */

#if !SOCKS_CLIENT && !HAVE_PRIVILEGES
   /*
    * uid, read from configfile.  But save old one first, in case we
    * need them to reopen logfiles.
    */

   olduserid = sockscf.uid;
   bzero(&sockscf.uid, sizeof(sockscf.uid));
#endif /* !SOCKS_CLIENT && !HAVE_PRIVILEGES */

   if (yyin == NULL
   ||  (stat(filename, &statbuf) == 0 && statbuf.st_size == 0)) {
      if (yyin == NULL)
         swarn("%s: could not open %s", function, filename);

      haveconfig            = 0;
      sockscf.option.debug  = 1;
   }
   else {
      slog(LOG_DEBUG, "%s: not parsing configfile %s (%s)",
                      function, filename,
                      yyin == NULL ? strerror(errno) : "zero-sized file");
      socks_parseinit = 0;
#if YYDEBUG
      yydebug         = 0;
#endif /* YYDEBUG */

      yylineno      = 1;
      errno         = 0;   /* don't report old errors in yyparse(). */
      haveconfig    = 1;

      parsingconfig = 1;
      yyparse();
      parsingconfig = 0;

#if SOCKS_CLIENT
      fclose(yyin);
#else
      /*
       * Leave it open so that if we get a sighup later, we are
       * always guaranteed to have a descriptor we can close/reopen
       * to parse the configfile.
       */
      sockscf.configfd = fileno(yyin);
#endif
   }

   errno = 0;
   return haveconfig ? 0 : -1;
}

void
yyerror(const char *fmt, ...)
{
   va_list ap;
   char buf[2048];
   size_t bufused;

   /* LINTED pointer casts may be troublesome */
   va_start(ap, fmt);

   if (parsingconfig)
      bufused = snprintfn(buf, sizeof(buf),
                          "%s: error on line %d, near \"%.20s\": ",
                          sockscf.option.configfile, yylineno,
                          (yytext == NULL || *yytext == NUL) ?
                          "'start of line'" : yytext);

   else
      bufused = snprintfn(buf, sizeof(buf), "error: ");

   vsnprintf(&buf[bufused], sizeof(buf) - bufused, fmt, ap);

   /* LINTED expression has null effect */
   va_end(ap);

   if (errno)
      serr(EXIT_FAILURE, "%s", buf);
   serrx(EXIT_FAILURE, "%s", buf);
}

void
yywarn(const char *fmt, ...)
{
   va_list ap;
   char buf[2048];
   size_t bufused;

   /* LINTED pointer casts may be troublesome */
   va_start(ap, fmt);

   if (parsingconfig)
      bufused = snprintfn(buf, sizeof(buf),
                         "%s: on line %d, near \"%.10s\": ",
                         sockscf.option.configfile, yylineno,
                         (yytext == NULL || *yytext == NUL) ?
                         "'start of line'" : yytext);
   else
      bufused = snprintfn(buf, sizeof(buf), "error: ");

   vsnprintf(&buf[bufused], sizeof(buf) - bufused, fmt, ap);

   /* LINTED expression has null effect */
   va_end(ap);

   if (errno)
      swarn("%s", buf);
   swarnx("%s", buf);
}

static void
addrinit(addr, _netmask_required)
   struct ruleaddr_t *addr;
   const int _netmask_required;
{

   atype            = &addr->atype;
   ipaddr           = &addr->addr.ipv4.ip;
   netmask          = &addr->addr.ipv4.mask;
   domain           = addr->addr.domain;
   ifname           = addr->addr.ifname;
   port_tcp         = &addr->port.tcp;
   port_udp         = &addr->port.udp;
   operator         = &addr->operator;

   netmask_required = _netmask_required;
   ruleaddr         = addr;
}

static void
gwaddrinit(addr)
   gwaddr_t *addr;
{
   static enum operator_t operatormem;

   atype    = &addr->atype;
   ipaddr   = &addr->addr.ipv4;
   domain   = addr->addr.domain;
   ifname   = addr->addr.ifname;
   url      = addr->addr.urlname;
   port_tcp = &addr->port;
   port_udp = &addr->port;
   operator = &operatormem; /* no operator in gwaddr and not used. */
}

#if SOCKS_CLIENT
static void
parseclientenv(haveproxyserver)
   int *haveproxyserver;
{
   const char *function = "parseclientenv()";
   char *proxyserver, *logfile, *debug, proxyservervis[256];

   if ((logfile = socks_getenv("SOCKS_LOGOUTPUT", dontcare)) != NULL)
      socks_addlogfile(&sockscf.log, logfile);

   if ((debug = socks_getenv("SOCKS_DEBUG", dontcare)) != NULL)
      sockscf.option.debug = atoi(debug);


   /*
    * Check if there is a proxyserver configured in the environment.
    * Initially assume there is none.
    */
   *haveproxyserver = 0;

   if ((proxyserver = socks_getenv(ENV_SOCKS4_SERVER, dontcare)) != NULL) {
      struct proxyprotocol_t proxyprotocol = { .socks_v4 = 1 };

      addproxyserver(proxyserver, &proxyprotocol);
      *haveproxyserver = 1;
   }

   if ((proxyserver = socks_getenv(ENV_SOCKS5_SERVER, dontcare)) != NULL) {
      struct proxyprotocol_t proxyprotocol = { .socks_v5 = 1 };

      addproxyserver(proxyserver, &proxyprotocol);
      *haveproxyserver = 1;
   }

   if ((proxyserver = socks_getenv(ENV_SOCKS_SERVER, dontcare)) != NULL) {
      struct proxyprotocol_t proxyprotocol = { .socks_v4 = 1, .socks_v5 = 1 };

      addproxyserver(proxyserver, &proxyprotocol);
      *haveproxyserver = 1;
   }

   if ((proxyserver = socks_getenv(ENV_HTTP_PROXY, dontcare)) != NULL) {
      struct proxyprotocol_t proxyprotocol = { .http = 1 };
      
      addproxyserver(proxyserver, &proxyprotocol);
      *haveproxyserver = 1;
   }

   if ((proxyserver = socks_getenv("UPNP_IGD", dontcare)) != NULL) {
      /*
       * Should be either an interface name (the interface to broadcast
       * for a response from the igd-device), "broadcast", to indicate
       * all interfaces, or a full url to the igd.
       */
      struct route_t route;

      bzero(&route, sizeof(route));
      route.gw.state.proxyprotocol.upnp = 1;

      str2vis(proxyserver,
              strlen(proxyserver),
              proxyservervis,
              sizeof(proxyservervis));

      route.src.atype                 = SOCKS_ADDR_IPV4;
      route.src.addr.ipv4.ip.s_addr   = htonl(0);
      route.src.addr.ipv4.mask.s_addr = htonl(0);
      route.src.port.tcp              = route.src.port.udp = htons(0);
      route.src.operator              = none;

      route.dst                       = route.src;

      /*
       * url or interface to broadcast for a response for?
       */
      if (strncasecmp(proxyserver, "http://", strlen("http://")) == 0) {
         route.gw.addr.atype = SOCKS_ADDR_URL;
         strncpy(route.gw.addr.addr.urlname, proxyserver,
                 sizeof(route.gw.addr.addr.urlname));

         if (route.gw.addr.addr.urlname[sizeof(route.gw.addr.addr.urlname) - 1]
         != NUL)
            serrx(EXIT_FAILURE, "url for igd, \"%s\", is too.  "
                                "Max is %lu characters",
                                proxyservervis,
                                (unsigned long)sizeof(
                                               route.gw.addr.addr.urlname) - 1);

         socks_addroute(&route, 1);
      }
      else if (strcasecmp(proxyserver, "broadcast") == 0) {
         /*
          * Don't know what interface the igd is on, so add routes
          * for it on all interfaces.  Hopefully at least one interface
          * will get a response.
          */
         struct ifaddrs *ifap, *iface;

         route.gw.addr.atype = SOCKS_ADDR_IFNAME;

         if (getifaddrs(&ifap) == -1)
            serr(EXIT_FAILURE, "%s: getifaddrs() failed to get interface list",
            function);

         for (iface = ifap; iface != NULL; iface = iface->ifa_next) {
            if (iface->ifa_addr                          == NULL
            ||  iface->ifa_addr->sa_family               != AF_INET
            ||  TOIN(iface->ifa_addr)->sin_addr.s_addr   == htonl(0)
            ||  !(iface->ifa_flags & (IFF_UP | IFF_MULTICAST))
            ||  iface->ifa_flags & (IFF_LOOPBACK | IFF_POINTOPOINT))
               continue;

            if (strlen(iface->ifa_name) > sizeof(route.gw.addr.addr.ifname) - 1)
            {
               serr(1, "%s: ifname %s is too long, max is %lu",
               function, iface->ifa_name,
               (unsigned long)(sizeof(route.gw.addr.addr.ifname) - 1));
            }

            strcpy(route.gw.addr.addr.ifname, iface->ifa_name);
            socks_addroute(&route, 1);
         }

         freeifaddrs(ifap);
      }
      else { /* must be an interface name. */
         /*
          * check that the given interface exists and has an address
          */
         struct sockaddr addr, mask;

         if (ifname2sockaddr(proxyserver, 0, &addr, &mask) == NULL)
            serr(1, "%s: can't find interface named %s with ip configured",
            function, proxyservervis);

         route.gw.addr.atype = SOCKS_ADDR_IFNAME;

         if (strlen(proxyserver) > sizeof(route.gw.addr.addr.ifname) - 1)
            serr(1, "%s: ifname %s is too long, max is %lu",
                    function,
                    proxyservervis,
                    (unsigned long)(sizeof(route.gw.addr.addr.ifname) - 1));

         strcpy(route.gw.addr.addr.ifname, proxyserver);

         socks_addroute(&route, 1);
      }

      *haveproxyserver = 1;
   }

   if (socks_getenv("SOCKS_AUTOADD_LANROUTES", isfalse) == NULL) {
      /*
       * assume it's good to add direct routes for the lan also.
       */
      struct ifaddrs *ifap;

      slog(LOG_DEBUG, "%s: auto-adding direct routes for lan ...", function);

      if (getifaddrs(&ifap) == 0) {
         struct ifaddrs *iface;

         for (iface = ifap; iface != NULL; iface = iface->ifa_next)
            if (iface->ifa_addr            != NULL
            &&  iface->ifa_addr->sa_family == AF_INET)
               socks_autoadd_directroute(
               (const struct sockaddr_in *)iface->ifa_addr,
               (const struct sockaddr_in *)iface->ifa_netmask);

         freeifaddrs(ifap);
      }
   }
   else
      slog(LOG_DEBUG, "%s: not auto-adding direct routes for lan", function);
}

static void
addproxyserver(proxyserver, proxyprotocol)
   const char *proxyserver;
   const struct proxyprotocol_t *proxyprotocol;
{
   const char *function = "addproxyserver()";
   struct sockaddr_in saddr;
   struct route_t route;
   struct ruleaddr_t raddr;
   char ipstring[INET_ADDRSTRLEN], *portstring, proxyservervis[256];

   bzero(&route, sizeof(route));
   route.gw.state.proxyprotocol = *proxyprotocol;

   str2vis(proxyserver,
           strlen(proxyserver),
           proxyservervis,
           sizeof(proxyservervis));

   slog(LOG_DEBUG,
        "%s: have a %s proxyserver set in environment, value %s",
        function,
        proxyprotocols2string(&route.gw.state.proxyprotocol, NULL, 0),
        proxyservervis);

   if (route.gw.state.proxyprotocol.http) {
      char emsg[256];

      if (urlstring2sockaddr(proxyserver,
                             (struct sockaddr *)&saddr,
                             emsg,
                             sizeof(emsg))
      == NULL) 
         serrx(EXIT_FAILURE,
               "%s: can't understand format of proxyserver %s: %s",
               function, proxyservervis, emsg);
               
   }
   else {
      if ((portstring = strchr(proxyserver, ':')) == NULL)
         serrx(EXIT_FAILURE, "%s: illegal format for port specification "
                             "in proxyserver %s: missing ':' delimiter",
                             function, proxyservervis);

      if (atoi(portstring + 1) < 1 || atoi(portstring + 1) > 0xffff)
         serrx(EXIT_FAILURE, "%s: illegal value (%d) for port specification "
                             "in proxyserver %s: must be between %d and %d",
                             function, atoi(portstring + 1),
                             proxyservervis, 1, 0xffff);

      if (portstring - proxyserver == 0
      || (size_t)(portstring - proxyserver) > sizeof(ipstring) - 1)
         serrx(EXIT_FAILURE,
               "%s: illegal format for ip address specification "
               "in proxyserver %s: too short/long",
               function, proxyservervis);

      strncpy(ipstring, proxyserver, (size_t)(portstring - proxyserver));
      ipstring[portstring - proxyserver] = NUL;
      ++portstring;

      bzero(&saddr, sizeof(saddr));
      saddr.sin_family = AF_INET;
      if (inet_pton(saddr.sin_family, ipstring, &saddr.sin_addr) != 1)
         serr(EXIT_FAILURE, "%s: illegal format for ip address "
                            "specification in proxyserver %s",
                            function, proxyservervis);
      saddr.sin_port = htons(atoi(portstring));
   }

   route.src.atype                           = SOCKS_ADDR_IPV4;
   route.src.addr.ipv4.ip.s_addr             = htonl(0);
   route.src.addr.ipv4.mask.s_addr           = htonl(0);
   route.src.port.tcp                        = route.src.port.udp = htons(0);
   route.src.operator                        = none;

   route.dst = route.src;

   ruleaddr2gwaddr(sockaddr2ruleaddr((struct sockaddr *)&saddr, &raddr),
   &route.gw.addr);

   socks_addroute(&route, 1);
}

#else /* !SOCKS_CLIENT */

static void
rulereset(void)
{

   timeout = &sockscf.timeout; /* default is global timeout, unless in a rule */
}

static void
ruleinit(rule)
   struct rule_t *rule;
{

   bzero(rule, sizeof(*rule));

   rule->linenumber        = yylineno;

   command                 = &rule->state.command;
   methodv                 = rule->state.methodv;
   methodc                 = &rule->state.methodc;
   protocol                = &rule->state.protocol;
   proxyprotocol           = &rule->state.proxyprotocol;
   timeout                 = &rule->timeout;
   *timeout                = sockscf.timeout; /* default values: as global. */

#if HAVE_GSSAPI
   gssapiservicename = rule->state.gssapiservicename;
   gssapikeytab      = rule->state.gssapikeytab;
   gssapiencryption  = &rule->state.gssapiencryption;
#endif /* HAVE_GSSAPI */

#if HAVE_LDAP
   ldap              = &rule->state.ldap;
#endif

   bzero(&src, sizeof(src));
   src.atype = SOCKS_ADDR_NOTSET;

   dst = rdr_from = rdr_to = src;

}

#endif /* !SOCKS_CLIENT */

