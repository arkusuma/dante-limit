/* A Bison parser, made by GNU Bison 2.5.  */

/* Bison interface for Yacc-like parsers in C
   
      Copyright (C) 1984, 1989-1990, 2000-2011 Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     SERVERCONFIG = 258,
     CLIENTCONFIG = 259,
     DEPRECATED = 260,
     CLIENTRULE = 261,
     INTERNAL = 262,
     EXTERNAL = 263,
     REALM = 264,
     REALNAME = 265,
     EXTERNAL_ROTATION = 266,
     SAMESAME = 267,
     DEBUGGING = 268,
     RESOLVEPROTOCOL = 269,
     SOCKET = 270,
     CLIENTSIDE_SOCKET = 271,
     SNDBUF = 272,
     RCVBUF = 273,
     SRCHOST = 274,
     NODNSMISMATCH = 275,
     NODNSUNKNOWN = 276,
     CHECKREPLYAUTH = 277,
     EXTENSION = 278,
     BIND = 279,
     PRIVILEGED = 280,
     IOTIMEOUT = 281,
     IOTIMEOUT_TCP = 282,
     IOTIMEOUT_UDP = 283,
     NEGOTIATETIMEOUT = 284,
     CONNECTTIMEOUT = 285,
     TCP_FIN_WAIT = 286,
     METHOD = 287,
     CLIENTMETHOD = 288,
     NONE = 289,
     GSSAPI = 290,
     UNAME = 291,
     RFC931 = 292,
     PAM = 293,
     BSDAUTH = 294,
     COMPATIBILITY = 295,
     SAMEPORT = 296,
     DRAFT_5_05 = 297,
     CLIENTCOMPATIBILITY = 298,
     NECGSSAPI = 299,
     USERNAME = 300,
     GROUPNAME = 301,
     USER_PRIVILEGED = 302,
     USER_UNPRIVILEGED = 303,
     USER_LIBWRAP = 304,
     LIBWRAP_FILE = 305,
     ERRORLOG = 306,
     LOGOUTPUT = 307,
     LOGFILE = 308,
     CHILD_MAXIDLE = 309,
     CHILD_MAXREQUESTS = 310,
     ROUTE = 311,
     VIA = 312,
     BADROUTE_EXPIRE = 313,
     MAXFAIL = 314,
     VERDICT_BLOCK = 315,
     VERDICT_PASS = 316,
     PAMSERVICENAME = 317,
     BSDAUTHSTYLENAME = 318,
     BSDAUTHSTYLE = 319,
     GSSAPISERVICE = 320,
     GSSAPIKEYTAB = 321,
     GSSAPIENCTYPE = 322,
     GSSAPIENC_ANY = 323,
     GSSAPIENC_CLEAR = 324,
     GSSAPIENC_INTEGRITY = 325,
     GSSAPIENC_CONFIDENTIALITY = 326,
     GSSAPIENC_PERMESSAGE = 327,
     GSSAPISERVICENAME = 328,
     GSSAPIKEYTABNAME = 329,
     PROTOCOL = 330,
     PROTOCOL_TCP = 331,
     PROTOCOL_UDP = 332,
     PROTOCOL_FAKE = 333,
     PROXYPROTOCOL = 334,
     PROXYPROTOCOL_SOCKS_V4 = 335,
     PROXYPROTOCOL_SOCKS_V5 = 336,
     PROXYPROTOCOL_HTTP = 337,
     PROXYPROTOCOL_UPNP = 338,
     USER = 339,
     GROUP = 340,
     COMMAND = 341,
     COMMAND_BIND = 342,
     COMMAND_CONNECT = 343,
     COMMAND_UDPASSOCIATE = 344,
     COMMAND_BINDREPLY = 345,
     COMMAND_UDPREPLY = 346,
     ACTION = 347,
     LINE = 348,
     LIBWRAPSTART = 349,
     LIBWRAP_ALLOW = 350,
     LIBWRAP_DENY = 351,
     LIBWRAP_HOSTS_ACCESS = 352,
     OPERATOR = 353,
     SOCKS_LOG = 354,
     SOCKS_LOG_CONNECT = 355,
     SOCKS_LOG_DATA = 356,
     SOCKS_LOG_DISCONNECT = 357,
     SOCKS_LOG_ERROR = 358,
     SOCKS_LOG_IOOPERATION = 359,
     IPADDRESS = 360,
     DOMAINNAME = 361,
     DIRECT = 362,
     IFNAME = 363,
     URL = 364,
     PORT = 365,
     SERVICENAME = 366,
     NUMBER = 367,
     FROM = 368,
     TO = 369,
     REDIRECT = 370,
     BANDWIDTH = 371,
     MAXSESSIONS = 372,
     UDPPORTRANGE = 373,
     UDPCONNECTDST = 374,
     YES = 375,
     NO = 376,
     BOUNCE = 377,
     LDAPURL = 378,
     LDAP_URL = 379,
     LDAPSSL = 380,
     LDAPCERTCHECK = 381,
     LDAPKEEPREALM = 382,
     LDAPBASEDN = 383,
     LDAP_BASEDN = 384,
     LDAPBASEDN_HEX = 385,
     LDAPBASEDN_HEX_ALL = 386,
     LDAPSERVER = 387,
     LDAPSERVER_NAME = 388,
     LDAPGROUP = 389,
     LDAPGROUP_NAME = 390,
     LDAPGROUP_HEX = 391,
     LDAPGROUP_HEX_ALL = 392,
     LDAPFILTER = 393,
     LDAPFILTER_AD = 394,
     LDAPFILTER_HEX = 395,
     LDAPFILTER_AD_HEX = 396,
     LDAPATTRIBUTE = 397,
     LDAPATTRIBUTE_AD = 398,
     LDAPATTRIBUTE_HEX = 399,
     LDAPATTRIBUTE_AD_HEX = 400,
     LDAPCERTFILE = 401,
     LDAPCERTPATH = 402,
     LDAPPORT = 403,
     LDAPPORTSSL = 404,
     LDAP_FILTER = 405,
     LDAP_ATTRIBUTE = 406,
     LDAP_CERTFILE = 407,
     LDAP_CERTPATH = 408,
     LDAPDOMAIN = 409,
     LDAP_DOMAIN = 410,
     LDAPTIMEOUT = 411,
     LDAPCACHE = 412,
     LDAPCACHEPOS = 413,
     LDAPCACHENEG = 414,
     LDAPKEYTAB = 415,
     LDAPKEYTABNAME = 416,
     LDAPDEADTIME = 417,
     LDAPDEBUG = 418,
     LDAPDEPTH = 419,
     LDAPAUTO = 420,
     LDAPSEARCHTIME = 421,
     AUTHSERVER = 422,
     AUTHUSERNAME = 423,
     AUTHPASSWORD = 424,
     AUTHDATABASE = 425,
     AUTHTABLE = 426,
     AUTHDELAY = 427,
     AUTH_STRING = 428
   };
#endif
/* Tokens.  */
#define SERVERCONFIG 258
#define CLIENTCONFIG 259
#define DEPRECATED 260
#define CLIENTRULE 261
#define INTERNAL 262
#define EXTERNAL 263
#define REALM 264
#define REALNAME 265
#define EXTERNAL_ROTATION 266
#define SAMESAME 267
#define DEBUGGING 268
#define RESOLVEPROTOCOL 269
#define SOCKET 270
#define CLIENTSIDE_SOCKET 271
#define SNDBUF 272
#define RCVBUF 273
#define SRCHOST 274
#define NODNSMISMATCH 275
#define NODNSUNKNOWN 276
#define CHECKREPLYAUTH 277
#define EXTENSION 278
#define BIND 279
#define PRIVILEGED 280
#define IOTIMEOUT 281
#define IOTIMEOUT_TCP 282
#define IOTIMEOUT_UDP 283
#define NEGOTIATETIMEOUT 284
#define CONNECTTIMEOUT 285
#define TCP_FIN_WAIT 286
#define METHOD 287
#define CLIENTMETHOD 288
#define NONE 289
#define GSSAPI 290
#define UNAME 291
#define RFC931 292
#define PAM 293
#define BSDAUTH 294
#define COMPATIBILITY 295
#define SAMEPORT 296
#define DRAFT_5_05 297
#define CLIENTCOMPATIBILITY 298
#define NECGSSAPI 299
#define USERNAME 300
#define GROUPNAME 301
#define USER_PRIVILEGED 302
#define USER_UNPRIVILEGED 303
#define USER_LIBWRAP 304
#define LIBWRAP_FILE 305
#define ERRORLOG 306
#define LOGOUTPUT 307
#define LOGFILE 308
#define CHILD_MAXIDLE 309
#define CHILD_MAXREQUESTS 310
#define ROUTE 311
#define VIA 312
#define BADROUTE_EXPIRE 313
#define MAXFAIL 314
#define VERDICT_BLOCK 315
#define VERDICT_PASS 316
#define PAMSERVICENAME 317
#define BSDAUTHSTYLENAME 318
#define BSDAUTHSTYLE 319
#define GSSAPISERVICE 320
#define GSSAPIKEYTAB 321
#define GSSAPIENCTYPE 322
#define GSSAPIENC_ANY 323
#define GSSAPIENC_CLEAR 324
#define GSSAPIENC_INTEGRITY 325
#define GSSAPIENC_CONFIDENTIALITY 326
#define GSSAPIENC_PERMESSAGE 327
#define GSSAPISERVICENAME 328
#define GSSAPIKEYTABNAME 329
#define PROTOCOL 330
#define PROTOCOL_TCP 331
#define PROTOCOL_UDP 332
#define PROTOCOL_FAKE 333
#define PROXYPROTOCOL 334
#define PROXYPROTOCOL_SOCKS_V4 335
#define PROXYPROTOCOL_SOCKS_V5 336
#define PROXYPROTOCOL_HTTP 337
#define PROXYPROTOCOL_UPNP 338
#define USER 339
#define GROUP 340
#define COMMAND 341
#define COMMAND_BIND 342
#define COMMAND_CONNECT 343
#define COMMAND_UDPASSOCIATE 344
#define COMMAND_BINDREPLY 345
#define COMMAND_UDPREPLY 346
#define ACTION 347
#define LINE 348
#define LIBWRAPSTART 349
#define LIBWRAP_ALLOW 350
#define LIBWRAP_DENY 351
#define LIBWRAP_HOSTS_ACCESS 352
#define OPERATOR 353
#define SOCKS_LOG 354
#define SOCKS_LOG_CONNECT 355
#define SOCKS_LOG_DATA 356
#define SOCKS_LOG_DISCONNECT 357
#define SOCKS_LOG_ERROR 358
#define SOCKS_LOG_IOOPERATION 359
#define IPADDRESS 360
#define DOMAINNAME 361
#define DIRECT 362
#define IFNAME 363
#define URL 364
#define PORT 365
#define SERVICENAME 366
#define NUMBER 367
#define FROM 368
#define TO 369
#define REDIRECT 370
#define BANDWIDTH 371
#define MAXSESSIONS 372
#define UDPPORTRANGE 373
#define UDPCONNECTDST 374
#define YES 375
#define NO 376
#define BOUNCE 377
#define LDAPURL 378
#define LDAP_URL 379
#define LDAPSSL 380
#define LDAPCERTCHECK 381
#define LDAPKEEPREALM 382
#define LDAPBASEDN 383
#define LDAP_BASEDN 384
#define LDAPBASEDN_HEX 385
#define LDAPBASEDN_HEX_ALL 386
#define LDAPSERVER 387
#define LDAPSERVER_NAME 388
#define LDAPGROUP 389
#define LDAPGROUP_NAME 390
#define LDAPGROUP_HEX 391
#define LDAPGROUP_HEX_ALL 392
#define LDAPFILTER 393
#define LDAPFILTER_AD 394
#define LDAPFILTER_HEX 395
#define LDAPFILTER_AD_HEX 396
#define LDAPATTRIBUTE 397
#define LDAPATTRIBUTE_AD 398
#define LDAPATTRIBUTE_HEX 399
#define LDAPATTRIBUTE_AD_HEX 400
#define LDAPCERTFILE 401
#define LDAPCERTPATH 402
#define LDAPPORT 403
#define LDAPPORTSSL 404
#define LDAP_FILTER 405
#define LDAP_ATTRIBUTE 406
#define LDAP_CERTFILE 407
#define LDAP_CERTPATH 408
#define LDAPDOMAIN 409
#define LDAP_DOMAIN 410
#define LDAPTIMEOUT 411
#define LDAPCACHE 412
#define LDAPCACHEPOS 413
#define LDAPCACHENEG 414
#define LDAPKEYTAB 415
#define LDAPKEYTABNAME 416
#define LDAPDEADTIME 417
#define LDAPDEBUG 418
#define LDAPDEPTH 419
#define LDAPAUTO 420
#define LDAPSEARCHTIME 421
#define AUTHSERVER 422
#define AUTHUSERNAME 423
#define AUTHPASSWORD 424
#define AUTHDATABASE 425
#define AUTHTABLE 426
#define AUTHDELAY 427
#define AUTH_STRING 428




#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{

/* Line 2068 of yacc.c  */
#line 190 "config_parse.y"

   char   *string;
   uid_t   uid;



/* Line 2068 of yacc.c  */
#line 403 "config_parse.h"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

extern YYSTYPE socks_yylval;


